<?php

return [
    "site" => env('SITE', ''), // 主站为空

    "site_type" => env('SITE_TYPE', 'independent'), // independent | complex

    "domain" => [
        "pc" => env('DOMAIN_PC'),
        "mobile" => env('DOMAIN_MOBILE'),
        "admin" => env('DOMAIN_ADMIN'),
        "image" => env("DOMAIN_IMAGE"),
    ],

    "scheme" => [
        "web" => env("SCHEME_WEB", "http"),
        "admin" => env("SCHEME_ADMIN", "http"),
        "image" => env("SCHEME_IMAGE", "http"),
    ],

    "title" => [
        "pc" => env('TITLE_PC', 'KiwiPc'),
        "mobile" => env('TITLE_MOBILE', 'KiwiMobile'),
        "admin" => env('TITLE_ADMIN', 'KiwiCore.'),
    ],

    /**
     * Cache 全局缓存时间
     */
    "global_cache_time" => env("GLOBAL_CACHE_TIME", 3600),

    /**
     * 皮肤
     */
    "skin" => env("SKIN", "yellow"),

    /**
     * 百度站长实时推送token
     */
    "bdzz_token" => env('BDZZ_TOKEN'),

    /**
     * 分页后地址是否斜线结尾
     */
    "paging_format" => env('PAGING_FORMAT', false),

    /**
     * 线上链接,带https://(百度站长实时推送使用)
     */
    "production" => [
        "pc" => env("PRODUCTION_PC"),
        "mobile" => env("PRODUCTION_MOBILE"),
    ],

];