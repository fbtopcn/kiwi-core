<?php

return [
    [
        "title" => "内容管理",
        "sub" => [
            [
                "title" => "文章管理",
                "url" => "/articles",
                "icon" => "pencil",
                "can" => "editor",
            ],
            [
                "title" => "创建文章",
                "url" => "/article/edit",
                "target" => "_blank",
                "icon" => "plus",
                "can" => "editor",
            ],
            [
                "title" => "频道",
                "url" => "/channels",
                "icon" => "folder-open",
                "can" => "manager"
            ],
            [
                "title" => "标签",
                "url" => "/tags",
                "icon" => "tags",
                "can" => "manager"
            ],
            [
                "title" => "文件上传",
                "url" => "/file/upload",
                "icon" => "upload",
            ],
            [
                "title" => "内容推荐",
                "url" => "/manualdatas",
                "icon" => "angle-double-up",
                "can" => "manager",
            ],
        ],
        "can" => "editor",
    ],
    [
        "title" => "通用设置",
        "sub" => [
            [
                "title" => "TDK",
                "url" => "/tdks",
                "icon" => "tumblr",
            ],
            [
                "title" => "友情链接",
                "url" => "/friendlinks",
                "icon" => "link",
            ],
            [
                "title" => "链接别名",
                "url" => "/aliaslinks",
                "icon" => "copy",
            ],
        ],
        "can" => "manager",
    ],
    [
        "title" => "账户设置",
        "sub" => [
            [
                "title" => "修改密码",
                "url" => "/auth/password/update",
                "icon" => "key",
            ],
        ]
    ],
    [
        "title" => "管理员设置",
        "sub" => [
            [
                "title" => "管理员",
                "url" => "/admins",
                "icon" => "user",
            ],
            [
                "title" => "操作日志",
                "url" => "/adminlogs",
                "icon" => "comment",
            ],
        ],
        "can" => "admin",
    ],
    [
        "title" => "Ants Collect",
        "sub" => [
            [
                "title" => "Task List",
                "url" => "/ants/task",
                "icon" => "pencil",
                "can" => "editor",
            ],
            [
                "title" => "Auto Published",
                "url" => "/ants/published",
                "icon" => "pencil",
                "can" => "editor",
            ]
        ],
        "can" => "manager"
    ],
    [
        "title" => "元数据设置",
        "sub" => [
            [
                "title" => "日志",
                "url" => "/logs",
                "icon" => "comment",
                "target" => "_blank",
                "can" => "admin",
            ],
            [
                "title" => "变量",
                "url" => "/variables",
                "icon" => "database",
                "can" => "manager"
            ],
        ],
        "can" => "manager",
    ],
];