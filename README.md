## 1. Installation

- Require the package using composer:

```
	"repositories": [
		{
			"type": "vcs",
			"url": ""
		}
	],
    "require": {
        "kiwi/kiwi-core": "dev-master"
    }
```


- Publish the public assets and config and views:

```
    php artisan vendor:publish --provider="KiwiCore\Providers\KiwiServiceProvider" --tag=assets
    php artisan vendor:publish --provider="KiwiCore\Providers\KiwiServiceProvider" --tag=config --force
    php artisan vendor:publish --provider="KiwiCore\Providers\KiwiServiceProvider" --tag=views
```

- Update Http/Kernel.php
- Update Exceptions/Handler.php
- Remove Providers\ All Provider, except yourself 
- Update app/config.php array of providers


## Deploy
- get code
```
    git clone XXX
```
- cd dir
- install dependence
```
    composer install
```
- update .env
- deploy paths.php
```
    php artisan vendor:publish --provider="KiwiCore\Providers\ServiceProvider" --tag=bootstrap
```
- update bootstrap/paths.php
```php
    return [
        "bootstrap_cache" => "/var/storage/NAME/cache",
        "log" => "/var/log/hyena/NAME.log",
        "storage" => "/var/storage/NAME/storage",
    ];
```
-   create dir
    - requirement
    ```
        sudo mkdir /var/storage
        sudo chown hyena:hyena /var/storage
        mkdir /var/storage/NAME
        
        sudo mkdir /var/log/hyena
        sudo chown hyena:hyena /var/log/hyena
    ```
    - storage
    ```
        cp -rf ./storage /var/storage/NAME
    ```
    - bootstrap cache
    ```
        mkdir /var/storage/NAME/cache
    ```
-   artisan
```
    php artisan config:cache
    php artisan route:cache
```

## 5. Update Deploy
- cd dir
- pull code
```
    git pull XXX
```
- install dependence
```
    composer install
```
- artisan
```
    php artisan config:cache
    php artisan route:cache
```

## 6. Other
- 四种模式， production,admin,test,local

## 异常处理

|                                  | local(debug=true) | test(debug=true) | admin or prod(debug=false) |
| -------------------------------- | ----------------- | ---------------- | -------------------------- |
| JSON(app,admin)                  |                   |                  |                            |
| CustomException                  | prepareResponse   | ->render         | ->render                   |
| AppException                     | prepareResponse   | ->render         | ->render                   |
| ValidationException              | prepareResponse   | {422,}           | {422,}                     |
| Exception                        | prepareResponse   | parent           | {500,}                     |
| HTML(web)                        |                   |                  |                            |
| CustomException                  | prepareResponse   | prepareResponse  | 500                        |
| AppException                     | prepareResponse   | prepareResponse  | 500                        |
| NotFoundException                | prepareResponse   | prepareResponse  | 404                        |
| ValidationException(admin login) | X                 | X                | X                          |
| Exception                        | prepareResponse   | prepareResponse  | 500                        |
| HTML(admin)                      |                   |                  |                            |
| CustomException                  | prepareResponse   | prepareResponse  | parent                     |
| AppException                     | prepareResponse   | prepareResponse  | parent                     |
| NotFoundException                | prepareResponse   | prepareResponse  | parent                     |
| ValidationException(admin login) | prepareResponse   | prepareResponse  | parent                     |
| Exception                        | prepareResponse   | prepareResponse  | parent                     |

