DROP TABLE IF EXISTS `ants_collect_data`;
CREATE TABLE `ants_collect_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL DEFAULT '1',
  `state` smallint(3) NOT NULL DEFAULT '0',
  `source_link` varchar(255) NOT NULL DEFAULT '',
  `published_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(200) NOT NULL DEFAULT '',
  `content` text,
  `message` varchar(255) NOT NULL DEFAULT '',
  `published_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `ants_collect_published`;
CREATE TABLE `ants_collect_published` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` tinyint(2) NOT NULL DEFAULT '2',
  `title` varchar(40) NOT NULL,
  `auto_published_cron` varchar(40) NOT NULL DEFAULT '',
  `auto_published_rules` text NOT NULL,
  `exclude_channels` varchar(255) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `ants_collect_task`;
CREATE TABLE `ants_collect_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` tinyint(5) NOT NULL DEFAULT '0',
  `title` varchar(40) NOT NULL,
  `address` varchar(255) NOT NULL DEFAULT '',
  `paging_address` varchar(255) NOT NULL DEFAULT '',
  `remove_head` tinyint(2) NOT NULL DEFAULT '1',
  `list_range` varchar(255) NOT NULL DEFAULT '',
  `list_rules` varchar(255) NOT NULL DEFAULT '',
  `details_range` varchar(255) NOT NULL DEFAULT '',
  `details_rules` varchar(255) NOT NULL DEFAULT '',
  `image_download` tinyint(2) NOT NULL DEFAULT '1',
  `image_attribute` varchar(10) NOT NULL DEFAULT 'src',
  `keywords_replace` text NOT NULL,
  `auto_collect_type` tinyint(2) NOT NULL DEFAULT '2',
  `auto_collect_cron` varchar(30) NOT NULL DEFAULT '',
  `auto_published_type` tinyint(2) NOT NULL DEFAULT '3',
  `auto_published_cron` varchar(30) NOT NULL DEFAULT '',
  `auto_published_rules` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;





ALTER TABLE `ants_collect_task` ADD COLUMN `auto_collect_type` tinyint(2) NOT NULL DEFAULT '2' after `keywords_replace`;
ALTER TABLE `ants_collect_task` ADD COLUMN `auto_collect_cron` varchar(30) NOT NULL DEFAULT '' after `auto_collect_type`;

ALTER TABLE `ants_collect_data` ADD COLUMN `published_id` int(11) NOT NULL DEFAULT 0 AFTER `source_link`;