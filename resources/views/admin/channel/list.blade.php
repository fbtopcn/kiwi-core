@extends("kiwi::common.list")

@section("title", "频道")

@var("model", "channel")

@section("content")
    <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a id="show_table" href="#tab_table" data-toggle="tab">表格显示</a></li>
                    <li><a id="show_tree" href="#tab_tree" data-toggle="tab">树形显示</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_table">
                        @var("index", "default")
                        @include("kiwi::common.list_html")
                    </div>
                    <div class="tab-pane" id="tab_tree">
                        <div id="tree" class="box-body"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <ul id="tree_menu" class="dropdown-menu" role="menu" aria-labelledby="dLabel">
        <li class="update"><a href="#update" style="color: #000;"><i class="icon-edit"></i>编辑</a></li>
        <li class="delete"><a href="#remove" style="color: #000;"><i class="icon-remove"></i>删除</a></li>
        <li class="divider"></li>
        <li class="create"><a href="#create" style="color: #000;"><i class="icon-plus"></i>添加</a></li>
    </ul>
@endsection

@push("style")
<link rel="stylesheet" href="{{cdn("/jqtree/1.4.0/jqtree.css")}}">
@endpush

@prepend("script")
<script src="{{cdn("/jqtree/1.4.0/tree.jquery.min.js")}}"></script>
<script src="{{asset("plugins/jqtree-contextmenu/jqTreeContextMenu.js")}}"></script>
<script>
    module.operator.create = function (channel) {
        var self = this;
        module.editor.create(function () {
            if ($("#tab_table").hasClass("active")) {
                self.reloadTable();
            } else {
                self.reloadTree();
            }
        });
        module.editor.setModel(channel);
    };
    module.operator.reloadTable = function () {
        this.table.reload();
    };
    module.operator.reloadTree = function () {
        var $tree = $("#tree");
        $tree.tree("destroy");
        tools.get("/api/channels-tree", null, function (data) {
            $tree.tree({
                data: data,
                autoOpen: true,
                dragAndDrop: false,
                useContextMenu: true
            });
            $tree.jqTreeContextMenu($('#tree_menu'), {
                "update": function (node) {
                    module.operator.update(node._id, function () {
                        module.operator.reloadTree();
                    });
                },
                "remove": function (node) {
                    module.operator.remove(node._id, function () {
                        module.operator.reloadTree();
                    });
                },
                "create": function (node) {
                    var channel = {parent: node.id};
                    module.operator.create(channel, function () {
                        module.operator.reloadTree();
                    });
                }
            });
        })
    };

    $(function () {
        $("#show_tree").click(function () {
            module.operator.reloadTree();
        });
        $("#show_table").click(function () {
            module.operator.reloadTable();
        });
    });
</script>
@endprepend

@include("kiwi::channel.edit")