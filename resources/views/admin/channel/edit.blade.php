@extends("kiwi::common.edit")

@section("form")
    <div class="input-group text_title">
        <label for="name">英文名称</label>
        <input id="name" name="name" type="text" class="form-control widthinu" required>
    </div>

    <div class="input-group text_title">
        <label for="title">中文名称</label>
        <input id="title" name="title" type="text" class="form-control widthinu" required>
    </div>

    <div class="input-group text_title">
        <label for="parent">父标签</label>
        <input id="parent" name="parent" type="text" class="form-control widthinu" readonly>
        <div id='parent_select' class='btn btn-primary btn-sm btn-flat dt-action'>选择</div>
        <div id='parent_clear' class='btn btn-primary btn-sm btn-flat dt-action'>清除</div>
    </div>

    <div class="input-group text_title">
        <label for="position">位置</label>
        <input id="position" name="position" type="text" class="form-control widthinu" required>
    </div>
@endsection

@prepend("script")
<script>
    module.editor.init = function () {
        var parent = $("#parent");
        $("#parent_select").click(function () {
            create_channel_selector(parent, function (name) {
                parent.val(name);
            });
        });

        $("#parent_clear").click(function () {
            parent.val("");
        });
    };
    module.editor.setModel = function (channel) {
        if (!channel) {
            return;
        }
        $("#name").val(channel.name);
        $("#title").val(channel.title);
        $("#parent").val(channel.parent);
        $("#position").val(channel.position);
    }
</script>
@endprepend

@include("kiwi::channel.tree")