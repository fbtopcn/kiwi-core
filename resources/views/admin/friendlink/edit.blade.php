@extends("kiwi::common.edit")

@section("form")
    <div class="input-group text_title">
        <label for="pattern">URL</label>
        <input id="pattern" name="pattern" type="text" class="form-control widthinu" required>
    </div>
    <div class="input-group text_title">
        <label for="domain">站点</label>
        <select id="domain" name="domain" class="form-control widthinu" required>
            <option value="1">PC</option>
            <option value="2">移动</option>
        </select>
    </div>
    <div class="input-group text_title">
        <label for="title">文字</label>
        <input id="title" name="title" type="text" class="form-control widthinu" required>
    </div>
    <div class="input-group text_title">
        <label for="link">链接</label>
        <input id="link" name="link" type="text" class="form-control widthinu" required>
    </div>
    <div class="input-group text_title">
        <label for="position">位置</label>
        <input id="position" name="position" type="text" class="form-control widthinu" value="1" required/>
    </div>
@endsection

@prepend("script")
<script>
    module.editor.setModel = function (model) {
        $("#title").val(model.title);
        $("#link").val(model.link);
        $("#position").val(model.position);
        $("#pattern").val(model.pattern);
        $("#domain").val(model.domain);
    }
</script>
@endprepend