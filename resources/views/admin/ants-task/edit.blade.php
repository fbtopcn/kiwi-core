@extends("kiwi::body")

@section("title", "编辑文章")

@section("body")
    <div class="content">
        <form id="form" class="form-horizontal" role="form">
            <div class="form-group">
                <label for="title" class="control-label col-sm-1">文章标题</label>
                <div class="col-sm-8">
                    <input id="title" name="title" type="text" class="form-control" required maxlength="50">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-1">缩略图</label>
                <input type='file' id='image-file' style='display: none'>
                <input type='hidden' id='image' name='image' required>
                <div class="col-sm-8">
                    <div id='image-button' class='btn btn-primary btn-sm btn-flat'>上传</div>
                    <div id='image-clear-button' class='btn btn-danger btn-sm btn-flat'>清除</div>
                    <br/>
                    <img width="100" style="padding-top: 10px" src='' id='image-image'/>
                </div>
            </div>

            <div class="form-group">
                <label for="editor" class="control-label col-sm-1">文章内容</label>
                <div class="col-sm-11">
                    <input id="editor" type="text" name="content" class="form-control" required/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <a id="save" class="btn btn-primary btn-sm btn-flat pull-right margin-r-5">保存文章</a>
                </div>
            </div>
            <input id="data_id" type="hidden" value="{{$data->id}}" />
        </form>
    </div>
@endsection

@prepend("script")
<script src="//cdn.ckeditor.com/4.7.3/full/ckeditor.js"></script>
<script>
    $(function () {
        // content
        var $editor = $("#editor");
        CKEDITOR.replace('editor', {
            filebrowserImageUploadUrl: "/api/ckeditor/upload",
            height: '700px'
        });
        CKEDITOR.editorConfig = function (config) {
            config.enterMode = CKEDITOR.ENTER_P;
        };
        CKEDITOR.instances.editor.on("key", function () {
            $editor.val(CKEDITOR.instances.editor.getData());
        });
        CKEDITOR.instances.editor.on("change", function () {
            $editor.val(CKEDITOR.instances.editor.getData());
        });
        CKEDITOR.instances.editor.on("instanceReady", function () {
            CKEDITOR.instances.editor.setData($editor.val());
        });

        var string = `{!! $data->content !!}`;
        CKEDITOR.instances.editor.setData(string);
        $editor.val(string);

        $("#title").val('{{$data->title}}');
        // image
        tools.initImageInput("image", "/api/file/upload/article");

        tools.setImageInputValue($("#image"), $("#image-image"), '{{image($data->image)}}');
    });

    $('#save').on('click', function () {
        var title = $("#title").val();
        var image = $("#image-image").attr('src');
        var content = $("#editor").val();
        var id = $('#data_id').val();
        tools.post("/api/ants/data/"+id+"/update", {
            'title': title,
            'image': image,
            'content': content,
        }, function () {
            window.close();
        });
    });
</script>
@endprepend