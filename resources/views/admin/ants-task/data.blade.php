@extends("kiwi::index")

@section("title", "任务详情")

@section("content")
    <div class="row">

        @include('kiwi::ants-task.layout.nav')

        <div class="col-md-9">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li><a href="/ants/task/{{$task->id}}">任务配置</a></li>
                    <li class="active"><a href="#timeline" data-toggle="tab">数据采集</a></li>
                    <li><a href="/ants/release/{{$task->id}}">数据发布</a></li>

                    <li class="pull-right">
                        <span class="collect_message"></span>
                        <img width="50" src="{{asset('ants/image/loading.gif')}}" style="display:none;" class="loading_img"/>
                        <button id="collect_play" type="button" class="btn btn-info ">采集列表</button>
                        <button id="collect_paging_page" type="button" class="btn btn-info ">采集分页</button>
                        <button id="collect_details" type="button" class="btn btn-warning ">采集详情</button>
                        <button id="collect_suspended" value="0" type="button" class="btn btn-danger ">停止</button>
                    </li>
                </ul>
                <div class="col-sm-6">
                    <div class="btn-group box-footer">
                        <button type="button" class="btn btn-info btn-sm" id="collect_state_complete" value="100">全部数据</button>
                        <button type="button" class="btn btn-info btn-sm dropdown-toggle" id="collect_state_complete_sub" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a class="state_change" data-state="{{\KiwiCore\Model\AntsCollectData::STATE_ALl}}">全部数据</a></li>
                            <li class="divider"></li>
                            <li><a class="state_change" data-state="{{\KiwiCore\Model\AntsCollectData::STATE_NO_COMPLETE}}">等待采集</a></li>
                            <li><a class="state_change" data-state="{{\KiwiCore\Model\AntsCollectData::STATE_COMPLETE}}">采集完成</a></li>
                            <li><a class="state_change" data-state="{{\KiwiCore\Model\AntsCollectData::STATE_FAIL}}">采集失败</a></li>
                            <li class="divider"></li>
                            <li><a class="state_change" data-state="{{\KiwiCore\Model\AntsCollectData::STATE_PUBLISHED}}">已发布</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">

                    <div class="box-footer clearfix">
                        <div class="pull-right" id="data_pagination_info">
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="active tab-pane" id="timeline">
                        <div class="">
                            <div class="box-body table-responsive no-padding" style="position: relative; overflow: auto; width: 100%;">
                                <table class="table table-hover" style="width: 1200px;">
                                    <tr>
                                        <th><input type="checkbox" class="batch_all_ids" /></th>
                                        <th>ID</th>
                                        <th>修改</th>
                                        <th>状态</th>
                                        <th>标题</th>
                                        <th>图片</th>
                                        <th>原文地址</th>
                                        <th>采集信息</th>
                                        <th>爬取时间</th>
                                    </tr>
                                    <tbody id="data_list"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin">
                            <li>
                                <select name="batch-operation-value" class="form-control">
                                    <option value="-1">选择操作</option>
                                    <option value="{{\KiwiCore\Model\AntsCollectData::STATE_NO_COMPLETE}}">设置为-待采集</option>
                                    <option value="{{\KiwiCore\Model\AntsCollectData::STATE_COMPLETE}}">设置为-采集成功</option>
                                    <option value="{{\KiwiCore\Model\AntsCollectData::STATE_PUBLISHED}}">设置为-已发布</option>
                                    <option value="{{\KiwiCore\Model\AntsCollectData::STATE_FAIL}}">设置为-采集失败</option>
                                    <option value="100">删除</option>
                                </select>
                            </li>
                        </ul>
                        <ul class="pagination pagination-sm no-margin">
                            <li>
                                <input type="button" id="batch-operation" class="btn btn-warning btn-sm" value="操作" />
                            </li>
                        </ul>
                        <ul class="pagination pagination-sm no-margin pull-right" id="data_pagination">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@prepend("script")
<script>

    // Data Auto Loading
    antsSmallBag.loadingList('/api/ants/data/{{$task->id}}/paging');

    // 终止详情采集
    $('#collect_suspended').on('click', function () {
        $('#collect_suspended').val(1);
        setTimeout(function(){
            $('.loading_img').hide();
            $('.collect_message').html('已停止采集.');
            antsSmallBag.updateStatistical({{$task->id}});
            antsSmallBag.changeDataColorState('/api/ants/data/{{$task->id}}/paging', {{\KiwiCore\Model\AntsCollectData::STATE_COMPLETE}});
        }, 1000);
    });

    // 列表翻页
    $('#data_pagination').on('click', 'li', function () {
        var url = $(this).children('a').attr('data-vel');
        var param = antsSmallBag.parseParams(url);

        antsSmallBag.loadingList('/api/ants/data/{{$task->id}}/paging', param.page, param.prePage);
    });

    // 类型筛选
    $('.state_change').on('click', function () {
        antsSmallBag.changeDataColorState('/api/ants/data/{{$task->id}}/paging', $(this).attr('data-state'));
    });

    // 批量操作文章
    $('#batch-operation').on('click', function () {
        var batch_ids =[];//定义一个数组
        $('input[name="batch_ids"]:checked').each(function(){
            batch_ids.push($(this).val());
        });

        var batch_operation = $('select[name="batch-operation-value"]').val();
        if (batch_operation == '-1'){
            antsSmallBag.alert('请选择操作类型.');
            return;
        }
        if (batch_ids.length == 0){
            antsSmallBag.alert('请点选要操作的数据.');
            return;
        }

        tools.post('/api/ants/data/{{$task->id}}/batchOperation', {
            'batchOperation': batch_operation,
            'batchIds': batch_ids,
        }, function (result) {
            // 展示已采集数据
            antsSmallBag.updateStatistical({{$task->id}});
            antsSmallBag.changeDataColorState('/api/ants/data/{{$task->id}}/paging', {{\KiwiCore\Model\AntsCollectData::STATE_ALl}});
        });
    });

    // 列表采集
    $('#collect_play').on('click', function () {
        $('.loading_img').toggle("slow");
        tools.get('/api/ants/task/{{$task->id}}/startJob', {}, function (result) {
            // 200为完成状态, 关闭动图
            if (result.code == 200){
                $('.loading_img').toggle("hide");
            }
            // 提示文字
            $('.collect_message').html(result.message);
            // 展示更新已采集数据
            antsSmallBag.updateStatistical({{$task->id}});
            antsSmallBag.changeDataColorState('/api/ants/data/{{$task->id}}/paging', {{\KiwiCore\Model\AntsCollectData::STATE_NO_COMPLETE}});
        });
    });

    //  启动分页采集
    $('#collect_paging_page').on('click', function () {
        var dlg = dialog({
            title: "编辑",
            content: $("#" + 'template_collect_paging').html(),
            okValue: '确定',
            ok: function () {
                $('.loading_img').toggle("slow");
                var pageNumber = $('#collect_page_number').val();
                dlg.remove();
                tools.get('/api/ants/task/{{$task->id}}/startJobPaging', {
                    'pageNumber': pageNumber,
                }, function (result) {
                    // 200为完成状态, 关闭动图
                    if (result.code == 200){
                        $('.loading_img').toggle("hide");
                    }
                    // 提示文字
                    $('.collect_message').html(result.message);

                    // 展示已采集数据
                    antsSmallBag.updateStatistical({{$task->id}});
                    antsSmallBag.changeDataColorState('/api/ants/data/{{$task->id}}/paging', {{\KiwiCore\Model\AntsCollectData::STATE_NO_COMPLETE}});
                });
            },
            cancelValue: '取消',
            cancel: function () {
                dlg.remove();
            }
        });
        dlg.showModal();
    });

    //  启动详情采集
    $('#collect_details').on('click', function () {
        var dlg = dialog({
            title: "详情配置",
            content: $("#" + 'template_collect_details').html(),
            okValue: '确定',
            ok: function () {
                $('.collect_message').html('正在采集详情, 请稍后.');
                $('.loading_img').toggle("slow");

                var step_length = $('#collect_details_step_length').val();
                var collect_details_model = $("input[name='collect_details_model']:checked").val();

                dlg.remove();
                $('#collect_suspended').val(0)
                autoJobDetails(step_length, collect_details_model);
            },
            cancelValue: '取消',
            cancel: function () {
                dlg.remove();
            }
        });
        dlg.showModal();

    });

    // 详情采集
    function autoJobDetails(step_length, collect_model){
        tools.get('/api/ants/task/{{$task->id}}/startJobDetails', {
            'stepLength': step_length,
        }, function (result) {
            // 展示目前采集进度信息
            $('.collect_message').html(result.message);

            // 更新页面 状态 + 数据
            if (collect_model == 1 || result.code == 200){
                antsSmallBag.updateStatistical({{$task->id}});
                antsSmallBag.changeDataColorState('/api/ants/data/{{$task->id}}/paging', {{\KiwiCore\Model\AntsCollectData::STATE_COMPLETE}});
            }

            if (result.code == 200){
                $('.loading_img').toggle("slow");
            } else {
                // 终止采集
                if (1 == $('#collect_suspended').val())
                    return;
                autoJobDetails(step_length, collect_model);
            }
        });
    }



</script>
<script type="text/html" id="template_collect_paging">
    <form id="form_editor_default" class="form-horizontal" role="form">
        <div class="input-group text_title">
            <label for="page">采集页码</label>
            <input id="collect_page_number" name="page" type="text" placeholder="2-3" class="form-control widthinu" required>
        </div>
    </form>
</script>
<script type="text/html" id="template_collect_details">
    <form id="form_editor_default" class="form-horizontal" role="form">
        <div class="input-group text_title">
            <label for="collect_details_model">采集模式</label>
            <label><input type="radio" name="collect_details_model" value="1" checked>清晰模式</label>
            <label><input type="radio" name="collect_details_model" value="2" >高性能模式</label>
        </div>
        <div class="input-group text_title">
            <label for="step_length">采集步长</label>
            <input id="collect_details_step_length" name="step_length" type="text" class="form-control widthinu" value="1" required>
        </div>
    </form>
</script>
@endprepend
