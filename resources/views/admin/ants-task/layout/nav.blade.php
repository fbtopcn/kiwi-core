<div class="col-md-3">

    <div class="box box-primary">
        <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle ants_spin ants_data_load" src="{{asset('ants/image/yuan'.($task->id%6).'.png')}}">

            <h3 class="profile-username text-center">{{$task->title}}</h3>

            <p class="text-muted text-center"><a href="/ants/task">蚂蚁采集</a></p>

            <div class=" btn-group btn-lg" style="width: 100%">
                <div class="col-xs-12">
                    <a href="/ants/task/{{$task->id}}" class="btn btn-info"><b>任务配置</b></a>
                    <a href="/ants/data/{{$task->id}}" class="btn btn-warning"><b>数据采集</b></a>
                    <a href="/ants/release/{{$task->id}}" class="btn btn-success"><b>数据发布</b></a>
                </div>
            </div>
            <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                    <b>等待采集</b> <a class="pull-right"><span class="label label-warning" id="statistical-0"></span></a>
                </li>
                <li class="list-group-item">
                    <b>采集完成</b> <a class="pull-right"><span class="label label-success" id="statistical-1"></span></a>
                </li>
                <li class="list-group-item">
                    <b>采集失败</b> <a class="pull-right"><span class="label label-danger" id="statistical-3"></span></a>
                </li>
                <li class="list-group-item">
                    <b>已发布</b> <a class="pull-right"><span class="label label-primary" id="statistical-2"></span></a>
                </li>
                <li class="list-group-item">
                    <b>总数据量</b> <a class="pull-right"><span class="label label-info" id="statistical-100"></span></a>
                </li>
            </ul>
            <img title="自动采集" id="collect_auto" class="ants_spin" width="40" src="{{asset('ants/image/taiji2.png')}}" style="cursor: pointer;">
            <img title="自动发布" id="collect_published" class="ants_spin" width="40" src="{{asset('ants/image/taiji3.png')}}" style="cursor: pointer;">
        </div>
        <!-- /.box-body -->
    </div>

    <!-- About Me Box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">调试模式</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <strong><i class="fa fa-book margin-r-5"></i> 地址</strong>

            <p class="text-muted">
                <input class="form-control" id="debug_address"/>
            </p>

            <hr>

            <label>
                <input type="radio" name="debug_remove_head" value="1" checked> 不删除(UTF-8编码推荐)
            </label>
            <label>
                <input type="radio" name="debug_remove_head" value="2"> 删除(GBK编码推荐)
            </label>

            <hr>
            <strong><i class="fa fa-pencil margin-r-5"></i> 范围</strong>

            <p class="text-muted">
                <input class="form-control" id="debug_range"/>
            </p>

            <hr>
            <strong><i class="fa fa-pencil margin-r-5"></i> 规则</strong>

            <p class="text-muted">
                <textarea class="form-control" id="debug_rules" placeholder="" rows="5"></textarea>
            </p>

            <hr>

            <button type="button" class="btn btn-block btn-warning btn-sm" id="debug_button">Debug</button>

            <div id="debug_result">
            </div>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
@prepend("script")
<script>
    // 更新任务最新状态
    antsSmallBag.updateStatistical({{$task->id}});

    // 定时采集
    $('#collect_auto').on('click', function () {
        var dlg = dialog({
            title: "定时采集配置",
            content: $("#" + 'template_collect_auto_collect').html(),
            okValue: '确定',
            ok: function () {
                var data = {
                    auto_collect_type: $("input[name='auto_collect_type']:checked").val(),
                    auto_collect_cron: $("input[name='auto_collect_cron']").val(),
                };
                tools.post('/api/ants/task/{{$task->id}}/updateCollect', data, function (result) {
                    if (result.code == 200){
                        antsSmallBag.alert(result.message+' 1.5秒后自动关闭~');
                        setTimeout(function () {
                            location.reload();
                        }, 1500);
                    } else {
                        antsSmallBag.errorAlert(result.message);
                    }
                });
            },
            cancelValue: '取消',
            cancel: function () {
                dlg.remove();
            }
        });
        dlg.showModal();
    });

    // 定时发布
    $('#collect_published').on('click', function () {
        antsSmallBag.alert('暂未开放！');
    });
</script>
<script type="text/html" id="template_collect_auto_collect">
    <form id="form_editor_default" class="form-horizontal" role="form">
        <div class="input-group text_title">
            <label for="auto_collect_type">自动采集</label>
            <label>
                <input type="radio" name="auto_collect_type" value="1" @if($task->auto_collect_type == \KiwiCore\Model\AntsCollectTask::AUTO_COLLECT_CUSTOM) checked @endif> 已启动
            </label>
            <label>
                <input type="radio" name="auto_collect_type" value="2" @if($task->auto_collect_type == \KiwiCore\Model\AntsCollectTask::AUTO_COLLECT_SHUTDOWN) checked @endif> 已关闭
            </label>
        </div>
        <div class="input-group text_title col-sm-12">
            <label for="title">定时表达式</label>
            <br />
            <input name="auto_collect_cron" type="text" class="form-control" size="10" value="@if(isset($task->auto_collect_cron)){{$task->auto_collect_cron}}@endif">
            <p><a href="http://www.matools.com/crontab" target="_blank">cron</a></p>
        </div>
    </form>
</script>
@endprepend