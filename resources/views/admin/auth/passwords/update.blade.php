@extends("kiwi::index")

@section("title", "修改密码")

@section("content")
    <div class="row">
        <form id="form" class="form-horizontal" role="form">
            {{csrf_field()}}
            <div class="form-group">
                <label for="old_password" class="control-label col-sm-1">旧密码</label>
                <div class="col-sm-3">
                    <input id="old_password" name="old_password" type="password" class="form-control" required
                           minlength="6">
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="control-label col-sm-1">新密码</label>
                <div class="col-sm-3">
                    <input id="password" name="password" type="password" class="form-control" required minlength="6">
                </div>
            </div>

            <div class="form-group">
                <label for="password_confirmation" class="control-label col-sm-1">确认密码</label>
                <div class="col-sm-3">
                    <input id="password_confirmation" name="password_confirmation" type="password" class="form-control"
                           required minlength="6">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-1">
                    <button id="save" class="btn btn-primary btn-sm btn-flat pull-right">修改</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@prepend("script")
<script>
    $(function () {
        tools.initAjaxForm($("#form"), "/api/auth/password/update", function () {
            window.location.href = "/";
        });
    });
</script>
@endprepend