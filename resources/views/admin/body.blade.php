<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield("title")_{{config("kiwi::title.admin")}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{cdn("/bootstrap/3.3.7/css/bootstrap.min.css")}}">
    <link rel="stylesheet" href="{{cdn("/font-awesome/4.7.0/css/font-awesome.min.css")}}">
    <link rel="stylesheet" href="{{cdn("/ionicons/2.0.1/css/ionicons.min.css")}}">
    <link rel="stylesheet" href="{{cdn("/datatables/1.10.15/css/dataTables.bootstrap.css")}}"/>
    <link rel="stylesheet" href="{{cdn("/select2/4.0.3/css/select2.min.css")}}">
    <link rel="stylesheet" href="{{cdn("/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css")}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{cdn("/admin-lte/2.3.11/css/AdminLTE.min.css")}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{cdn("/admin-lte/2.3.11/css/skins/skin-" . config("kiwi.skin") . ".min.css")}}">
    <!--our style-->
    <link rel="stylesheet" href="{{asset("dist/css/style.css")}}">
    <link rel="stylesheet" href="{{asset("ants/css/ants.css")}}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @stack('style')
</head>
<body class="hold-transition skin-{{config("kiwi.skin")}} sidebar-mini fixed collapse_sidebar @yield("body_class")">
@yield("body")
<script src="{{cdn("/jquery/3.2.1/jquery.min.js")}}"></script>
<script src="{{cdn("/bootstrap/3.3.7/js/bootstrap.min.js")}}"></script>
<script src="{{cdn("/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js")}}"></script>
<script src="{{cdn("/fastclick/1.0.6/fastclick.min.js")}}"></script>
<script src="{{cdn("/datatables/1.10.15/js/jquery.dataTables.min.js")}}"></script>
<script src="{{cdn("/datatables/1.10.15/js/dataTables.bootstrap.min.js")}}"></script>
<script src="{{cdn("/jquery-validate/1.16.0/jquery.validate.min.js")}}"></script>
<script src="{{cdn("/select2/4.0.3/js/select2.min.js")}}"></script>
<script src="{{cdn("/select2/4.0.3/js/i18n/zh-CN.js")}}"></script>
<script src="{{cdn("/artDialog/7.0.0/dialog.js")}}"></script>
<script src="{{cdn("/admin-lte/2.3.11/js/app.min.js")}}"></script>
<script src="{{cdn("/lodash.js/4.16.4/lodash.min.js")}}"></script>
<script src="{{cdn("/moment.js/2.22.1/moment.min.js")}}"></script>
<script src="{{cdn("/moment.js/2.22.1/locale/zh-cn.js")}}"></script>
<script src="{{cdn("/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js")}}"></script>

<script src="{{asset("dist/js/tools.js")}}"></script>
<script src="{{asset("dist/js/datatable.js")}}"></script>
<script src="{{asset("dist/js/admin.js")}}"></script>

<script src="{{asset("ants/js/ants-small-bag.js")}}"></script>
<script src="{{asset("ants/js/ants.js")}}"></script>
<script>
    var module = {};
</script>
@stack('script')
</body>
</html>
