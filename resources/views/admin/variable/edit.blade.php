@extends("kiwi::common.edit")

@section("form")
    <div class="input-group text_title">
        <label for="name">名称</label>
        <input id="name" name="name" type="text" class="form-control widthinu" required>
    </div>
    <div class="input-group text_title">
        <label for="type">数据类型</label>
        <input id="type" name="type" type="text" class="form-control widthinu" required>
        <span>1:string; 2:int; 3:json;</span>
    </div>
    <div class="input-group text_title">
        <label for="description">描述</label>
        <input id="description" name="description" type="text" class="form-control widthinu">
    </div>
    <div class="input-group text_title">
        <label for="value">值</label>
        <textarea id="value" name="value" class="form-control widthinu" rows="10" required></textarea>
    </div>
@endsection

@prepend("script")
<script>
    module.editor.setModel = function (model) {
        $("#name").val(model.name);
        $("#type").val(model.type);
        $("#value").val(model.value);
        $("#description").val(model.description);
    }
</script>
@endprepend