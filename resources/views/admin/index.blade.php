@extends("kiwi::body")

@section("body")
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="/" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>EL</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Hyena</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- 退出登录-->
                        <li>
                            <a href="javascript:;" title="{{Auth::user()->name}}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-fw fa-power-off"></i>退出登录
                            </a>
                            <form id="logout-form" action="{{url("/auth/logout")}}" method="POST"
                                  style="display: none;">
                                {{csrf_field()}}
                            </form>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    @foreach($mainMenu as $head)
                        <li class="header">{{$head->title}}</li>
                        @foreach($head->sub as $item)
                            <li>
                                <a href="{{url($item->url)}}"
                                   target="@if(isset($item->target) && $item->target == '_blank')_blank @else_self @endif">
                                    <i class="fa fa-fw fa-{{$item->icon}}"></i>
                                    <span>{{$item->title}}</span>
                                </a>
                            </li>
                        @endforeach
                    @endforeach
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    @yield("title")
                    <small>@yield("title-desc" or "")</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="/"><i class="fa fa-dashboard"></i> 首页</a></li>
                    <li class="active">@yield("title")</li>
                </ol>
            </section>
            <section class="content">
                @yield("content")
            </section>
        </div>
    </div>
@endsection

@prepend("script")
<script>
    $(".sidebar-menu>li").each(function () {
        var $this = $(this);
        var href = $("a", $this).attr("href");
        if (href === location.href) {
            $(this).addClass("active");
        }
    });
</script>
@endprepend