@prepend("script")
<script type="text/html" id="template_editor_default">
    <form id="form_editor_default" class="form-horizontal" role="form">
        {{csrf_field()}}
        @yield("form")

        <div class="submit_fab">
            <button type="submit" class="btn btn-primary btn-sm btn-flat">确定</button>
        </div>
    </form>
</script>
@endprepend