@extends("kiwi::index")

@section("content")
    @include("kiwi::common.list_html", ["index" => "default"])
@endsection

@prepend("script")
<script>
    var site = "{{$site}}";
    var model = "{{$model}}";
    var columns = JSON.parse('{!! $columns !!}');
    var filters = JSON.parse('{!! $filters !!}');

    module.urlFactory = {
        list: function () {
            return admin.addSiteQueryString('/api/' + model + 's', site);
        },
        info: function (id) {
            return admin.addSiteQueryString("/api/" + model + "/" + id, site);
        },
        create: function () {
            return admin.addSiteQueryString("/api/" + model + "/create", site);
        },
        update: function (id) {
            return admin.addSiteQueryString("/api/" + model + "/" + id + "/update", site);
        },
        remove: function (id) {
            return admin.addSiteQueryString("/api/" + model + "/" + id + "/delete", site);
        }
    };
    module.$table = $(".data-table[data-index=default]").first();
    module.editor = admin.createEditor(module.urlFactory, "template_editor_default", "form_editor_default");
    module.operator = admin.createOperator(module.urlFactory, module.editor);
    module.table = admin.createDataTable(module.$table, columns, filters, module.operator, module.urlFactory.list());
</script>
@endprepend
