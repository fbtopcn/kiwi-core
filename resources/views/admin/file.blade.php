@extends("kiwi::index")

@section("title", "上传文件")

@section("content")
    <div class="content">
        <form id="form" class="form-horizontal" role="form">
            <div class="input-group text_title">
                <label for="type">目录</label>
                <input id="type" name="type" type="text" class="form-control widthinu" required>
            </div>

            <div class="input-group text_title">
                <label for="name">文件名</label>
                <input id="name" name="name" type="text" class="form-control widthinu" required>
            </div>

            <input type='file' id='file' style='display: none'>
            <div id='submit' class='btn btn-primary btn-sm btn-flat'>上传</div>
        </form>

        <div class="input-group text_title">
            <label for="url">url</label>
            <input id="url" type="text" class="form-control widthinu">
        </div>
    </div>
@endsection

@push("script")
<script>
    $(function () {
        var fileInput = $('#file');
        var uploadImage = function () {
            var type = $("#type").val();
            var name = $("#name").val();
            if (!type || !name) {
                alert("请填写目录和名称");
                return;
            }
            var url = "/api/file/upload/" + type + "/" + name;

            var data = new FormData();
            var files = fileInput[0].files;
            if (files) {
                data.append("file", files[0]);
                tools.uploadFile(url, data, function (result) {
                    alert("上传成功");
                    console.log(result);
                    $("#url").val(result.url);
                    //location.href = "/file/upload";
                }, function (err) {
                    return tools.errorAlert('上传失败');
                });
            } else {
                return tools.errorAlert('本地文件不存在');
            }
        };

        $('#submit').on('click', function () {
            fileInput.click();
        });
        fileInput.on('change', function () {
            uploadImage();
        });
    });
</script>
@endpush