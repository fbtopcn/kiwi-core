@extends("kiwi::common.edit")

@section("form")
    <div class="form-group">
        <label for="name" class="control-label col-sm-3">用户名</label>
        <div class="col-sm-9">
            <input id="name" name="name" type="text" class="form-control" required>
        </div>
    </div>

    <div class="form-group">
        <label for="email" class="control-label col-sm-3">邮箱</label>
        <div class="col-sm-9">
            <input id="email" name="email" type="text" class="form-control" required>
        </div>
    </div>

    <div class="form-group">
        <label for="role" class="control-label col-sm-3">权限</label>
        <div class="col-sm-9">
            <select id="role" name="role" class="form-control">
                <option value="admin">admin</option>
                <option value="manager">manager</option>
                <option value="editor">editor</option>
            </select>
        </div>
    </div>
@endsection

@prepend("script")
<script>
    module.editor.setModel = function (model) {
        $("#name").val(model.name);
        $("#email").val(model.email);
    }
</script>
@endprepend