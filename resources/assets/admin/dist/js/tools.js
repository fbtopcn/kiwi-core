'use strict';

// lodash setting
_.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
};

var tools = {};

tools.formatDateTime = function (value) {
    return value ? moment(value).format("YYYY-MM-DD HH:mm:ss") : '';
};

tools.getEnumText = function (value, list) {
    for (var i = 0; i < list.length; ++i) {
        if (list[i].value === value) {
            return list[i].text;
        }
    }
    return '';
};

tools.extendObject = function (dest, src) {
    src = src || {};
    dest = dest || {};
    for (var key in src) {
        if (src.hasOwnProperty(key)) {
            dest[key] = src[key];
        }
    }
    return dest;
};

tools.isEmptyObject = function (obj) {
    for (var i in obj) {
        return false;
    }
    return true;
};

/**
 * 取url参数
 * @param name
 * @returns {*}
 */
tools.getQueryStr = function (name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r !== null) return decodeURI(r[2]);
    return null; //返回参数值
};

/**
 * 添加url参数
 */
tools.addQueryStr = function (url, key, value) {
    var segmentIndex = url.indexOf("#");
    var segment = (segmentIndex === -1) ? "" : url.substr(segmentIndex);
    var left = (segmentIndex === -1) ? url : url.substr(0, segmentIndex);
    left += (left.indexOf("?") === -1) ? "?" : "&";
    left += key + "=" + encodeURIComponent(value);
    return left + segment;
};

/**
 * 创建下拉选择框
 * @param dom
 * @param list
 * @param canEmpty 是否允许为空时的值，undefined为不可为空
 * @param selectValue
 */
tools.initSelect = function (dom, list, canEmpty, selectValue) {
    dom.empty();

    if (canEmpty) {
        dom.append("<option value=''>——请选择——</option>");
    }
    list.forEach(function (item) {
        dom.append("<option value='" + item.name + "'>" + (item.title) + "</option>");
    });
    if (selectValue) {
        var option = dom.children("option[value=" + selectValue + "]");
        if (option) {
            option.selected = true;
        }
    }
};

/**
 * 上传文件
 * @param url
 * @param fileData
 * @param successCallback
 * @param failCallback
 */
tools.uploadFile = function (url, fileData, successCallback, failCallback) {
    tools.startLoading();
    $.ajax({
        url: url,
        type: 'POST',
        data: fileData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (obj) {
            tools.stopLoading();
            if (obj.status === 0) {
                return successCallback & successCallback(obj.data);
            } else {
                return failCallback && failCallback(obj);
            }
        },
        error: function (err) {
            tools.stopLoading();
            return failCallback && failCallback(err);
        }
    });
};

/**
 * post请求,json格式
 * @param url
 * @param data
 * @param successCallback
 * @param failCallback
 */
tools.post = function (url, data, successCallback, failCallback) {
    this.ajax({
        type: 'post',
        data: data,
        dataType: 'json',
        url: url
    }, successCallback, failCallback);
};

/**
 * get请求,form格式
 * @param url
 * @param data
 * @param successCallback
 * @param failCallback
 */
tools.get = function (url, data, successCallback, failCallback) {
    this.ajax({
        type: 'get',
        data: data,
        url: url
    }, successCallback, failCallback);
};

/**
 * ajax请求
 * @param options
 * @param successCallback
 * @param failCallback
 */
tools.ajax = function (options, successCallback, failCallback) {
    tools.startLoading();
    $.ajax(options).done(function (msg, text, req) {
        tools.stopLoading();
        if (msg.status === 0) {
            successCallback && successCallback(msg.data);
        } else {
            if (failCallback) {
                failCallback(msg.status, msg.message);
            } else {
                tools.errorAlert('失败,code:' + msg.status + ',message:' + msg.message);
            }
        }
    }).fail(function (req, text, e) {
        tools.stopLoading();
        if (failCallback) {
            failCallback(500, text ? text : "网络错误");
        } else {
            tools.errorAlert('网络错误,失败,message:' + text);
            // tools.errorAlert('网络错误,失败,message:' + eval(req.responseJSON).errors.title[0]);
        }
    });
};

/**
 * 错误提示
 * @param message
 */
tools.errorAlert = function (message) {
    console.log(message);
    var d = dialog({
        title: "错误",
        content: message
    });
    d.showModal();
};

tools.alert = function (message) {
    var d = dialog({
        title: "信息",
        content: message
    });
    d.showModal();
};

tools.startLoading = function () {
    // TODO
};

tools.stopLoading = function () {
    // TODO
};


/**
 * 初始化图片上传控件
 * @param id
 * @param url
 */
// TODO not use id
tools.initImageInput = function (id, url) {
    var input = $('#' + id);
    var fileInput = $('#' + id + '-file');
    var uploadButton = $('#' + id + '-button');
    var clearButton = $('#' + id + '-clear-button');
    var image = $('#' + id + '-image');

    var uploadImage = function () {
        var data = new FormData();
        var files = fileInput[0].files;
        if (files) {
            data.append("file", files[0]);
            tools.uploadFile(url, data, function (reply) {
                image.css('display', 'block');
                image.attr('src', reply.url);
                input.val(reply.url);
            }, function (err) {
                return tools.errorAlert('图片上传失败');
            });
        } else {
            return tools.errorAlert('本地图片不存在');
        }
    };

    uploadButton.on('click', function () {
        fileInput.click();
    });
    clearButton.on('click', function () {
        input.val('');
        image.css('display', 'none');
        image.attr('src', '');
        fileInput.val('');
    });
    fileInput.on('change', function () {
        uploadImage();
    });

    image.css('display', 'none');
};

/**
 * 填充值
 * @param input
 * @param image
 * @param url
 */
tools.setImageInputValue = function (input, image, url) {
    input.val(url);
    image.css('display', 'block');
    image.attr('src', url);
};

/**
 * 初始化ajax表单
 * @param form
 * @param submitUrl
 * @param successCallback
 * @param failCallback
 */
tools.initAjaxForm = function (form, submitUrl, successCallback, failCallback) {
    form.submit(function () {
        var url = "";
        if (typeof submitUrl === "string") {
            url = submitUrl;
        } else if (typeof submitUrl === "function") {
            url = submitUrl();
        } else {
            return false;
        }
        $(":disabled", form).removeAttr('disabled');
        tools.post(url, form.serialize(), successCallback, failCallback);
        return false;
    });

    // form.validate({
    //     ignore: [],     // 验证隐藏域
    //     submitHandler: function () {
    //         // submitHandler 似乎是代替了 form.submit  return false的逻辑
    //         tools.post(submitUrl, form.serialize(), successCallback);
    //     }
    // });
};
