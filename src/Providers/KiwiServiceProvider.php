<?php
namespace KiwiCore\Providers;

use Illuminate\Support\Facades\Gate;
use KiwiCore\Foundation\Environment;
use KiwiCore\Service\LinkFormat;
use KiwiCore\Service\BladeView;
use KiwiCore\Utils\BeautifyUrlLengthAwarePaginator;

class KiwiServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const ROLES = [
        "admin" => 1,
        "manager" => 2,
        "editor" => 3,
    ];

    public function register()
    {
		$this->loadRoutes();

		$this->publishAssets();

		$this->registerViews();

		$this->publishConfig();
    }

    public function boot()
    {
        LinkFormat::boot();

        BladeView::boot();

        // 分页
        BeautifyUrlLengthAwarePaginator::injectIntoBuilder();

        $this->bootGate();
    }

    private function packagePath($path)
    {
        return __DIR__ . "/../../$path";
    }

    private function bootGate(){
        if (Environment::isAdmin()) {
            collect(["admin", "manager", "editor"])->each(function ($role) {
                Gate::define($role, function ($user) use ($role) {
                    return self::ROLES[$user->role] <= self::ROLES[$role];
                });
            });
        }
    }

    private function publishAssets()
    {
        $this->publishes([
            $this->packagePath("resources/assets") => public_path(""),
        ], "assets");
    }

    private function registerViews()
    {
        if (Environment::isAdmin()) {
            $viewsPath = $this->packagePath("resources/views/admin");
            $this->loadViewsFrom($viewsPath, "kiwi");
        }
    }

    private function publishConfig()
    {
        $this->publishes([
            $this->packagePath("config") => config_path(),
        ], "config");
    }

    private function loadRoutes()
    {
        $this->loadRoutesFrom($this->packagePath("routes/loader.php"));
    }

}