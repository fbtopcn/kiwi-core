<?php

namespace KiwiCore\Http\Middleware;

use Closure;
use KiwiCore\Foundation\Environment;
use Illuminate\Http\Request;
use Illuminate\View\Factory;

class ViewLocation
{
	private $viewFactory;

	public function __construct(Factory $viewFactory)
	{
		$this->viewFactory = $viewFactory;
	}

	public function handle(Request $request, Closure $next)
	{
        $path = rtrim("/views/".config('kiwi.site'), '/');

		if (Environment::isPC()) {
			$viewPath = resource_path("$path/pc");
		}
		if (Environment::isMobile()) {
			$viewPath = resource_path("$path/m");
		}
		if (Environment::isAdmin()) {
			$viewPath = resource_path("$path/admin");
		}

		if (!empty($viewPath)) {
			$this->viewFactory->addLocation($viewPath);
		}

		return $next($request);
	}
}