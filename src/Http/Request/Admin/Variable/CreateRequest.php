<?php

namespace KiwiCore\Http\Request\Admin\Variable;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\Variable;

class CreateRequest extends Request
{
	use Rule;

	public function model()
	{
		return $this->_model(new Variable());
	}
}