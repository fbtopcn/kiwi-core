<?php

namespace KiwiCore\Http\Request\Admin\Variable;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\Variable;

class UpdateRequest extends Request
{
	use Rule;

	public function model(Variable $model)
	{
		return $this->_model($model);
	}
}