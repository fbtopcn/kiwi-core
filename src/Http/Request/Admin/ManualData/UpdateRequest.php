<?php

namespace KiwiCore\Http\Request\Admin\ManualData;

use KiwiCore\Model\ManualData;
use KiwiCore\Http\Request\Request;

class UpdateRequest extends Request
{
	use Rule;

	public function model(ManualData $model)
	{
		return $this->_model($model);
	}
}