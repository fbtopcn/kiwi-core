<?php

namespace KiwiCore\Http\Request\Admin\FriendLink;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\FriendLink;

class UpdateRequest extends Request
{
	use Rule;

	public function model(FriendLink $model)
	{
		return $this->_model($model);
	}
}