<?php

namespace KiwiCore\Http\Request\Admin\Channel;

use KiwiCore\Model\Channel;

trait Rules
{
	public function rules()
	{
		return [
			"name" => "required|max:50",
			"title" => "required|string|max:10",
//			"parent" => "required|string",
			"position" => "required|int",
		];
	}

	protected function _model(Channel $model)
	{
		$name = $this->input("name");
		$model->name = str_replace(" ", "-", strtolower($name));
		$model->title = $this->input("title");
		$model->parent = $this->input("parent");
		$model->position = intval($this->input("position"));
		return $model;
	}
}