<?php

namespace KiwiCore\Http\Request\Admin\Channel;

use KiwiCore\Model\Channel;
use KiwiCore\Http\Request\Request;

class CreateRequest extends Request
{
	use Rules;

	public function model()
	{
		return $this->_model(new Channel());
	}
}