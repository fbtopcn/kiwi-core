<?php

namespace KiwiCore\Http\Request\Admin\AntsCollectPublished;

use KiwiCore\Model\AntsCollectPublished;
use KiwiCore\Http\Request\Request;

class UpdateRequest extends Request
{
	use Rules;

	public function model(AntsCollectPublished $model)
	{
		return $this->_model($model);
	}
}