<?php

namespace KiwiCore\Http\Request\Admin\AntsCollectPublished;

use KiwiCore\Model\AntsCollectPublished;
use KiwiCore\Http\Request\Request;

class CreateRequest extends Request
{
	use Rules;

	public function model()
	{
		return $this->_model(new AntsCollectPublished());
	}
}