<?php

namespace KiwiCore\Http\Request\Admin\Admin;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\Admin;

class CreateRequest extends Request
{
	use Rule;

	public function model()
	{
		return $this->_model(new Admin());
	}
}