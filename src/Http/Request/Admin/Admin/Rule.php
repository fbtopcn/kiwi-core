<?php

namespace KiwiCore\Http\Request\Admin\Admin;


use KiwiCore\Model\Admin;

trait Rule
{
	public function rules()
	{
		return [
			"name" => "required|max:255",
			"email" => "required|email|max:255",
			"role" => "required|string|in:admin,manager,editor"
		];
	}

	public function messages()
	{
		return [
			"name.required" => "必须填写名称"
		];
	}

	protected function _model(Admin $model)
	{
		$model->name = $this->input("name");
		$model->email = $this->input("email");
		$model->role = $this->input("role");
		return $model;
	}
}