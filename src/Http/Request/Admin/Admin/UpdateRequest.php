<?php

namespace KiwiCore\Http\Request\Admin\Admin;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\Admin;

class UpdateRequest extends Request
{
	use Rule;

	public function model(Admin $model)
	{
		return $this->_model($model);
	}
}