<?php

namespace KiwiCore\Http\Request\Admin\AliasLink;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\AliasLink;

class UpdateRequest extends Request
{
	use Rule;

	public function model(AliasLink $model)
	{
		return $this->_model($model);
	}
}