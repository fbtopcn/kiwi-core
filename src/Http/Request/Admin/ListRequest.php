<?php

namespace KiwiCore\Http\Request\Admin;

use KiwiCore\Http\Request\Request;

class ListRequest extends Request
{
	public function rules()
	{
		return [
			"start" => "required|integer|min:0",
			"length" => "required|integer|max:100"
		];
	}

	public function start()
	{
		return $this->input("start");
	}

	public function length()
	{
		return $this->input("length");
	}
}