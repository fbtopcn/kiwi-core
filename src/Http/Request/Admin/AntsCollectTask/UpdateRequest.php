<?php

namespace KiwiCore\Http\Request\Admin\AntsCollectTask;

use KiwiCore\Model\AntsCollectTask;
use KiwiCore\Http\Request\Request;

class UpdateRequest extends Request
{
	use Rules;

	public function model(AntsCollectTask $model)
	{
		return $this->_model($model);
	}
}