<?php

namespace KiwiCore\Http\Request\Admin\AntsCollectTask;

use KiwiCore\Model\AntsCollectTask;

trait RulesPublished
{
	public function rules()
	{
		return [
			"auto_published_type" => "required|max:3",
			"auto_published_cron" => "required|string|max:40",
			"auto_published_rules" => "required|string",
		];
	}

    protected function _modelPublished(AntsCollectTask $model)
    {
        $model->auto_published_type = $this->input("auto_published_type");
        $model->auto_published_cron = $this->input("auto_published_cron");
        $model->auto_published_rules = $this->input("auto_published_rules");
        return $model;
    }
}