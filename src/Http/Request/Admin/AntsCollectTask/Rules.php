<?php

namespace KiwiCore\Http\Request\Admin\AntsCollectTask;

use KiwiCore\Model\AntsCollectTask;

trait Rules
{
	public function rules()
	{
		return [
			"title" => "required|max:40",
			"address" => "required|string|max:255",
			"paging_address" => "string|max:255",
			"remove_head" => "required|string|max:2",
			"list_range" => "required|string|max:255",
			"list_rules" => "required|string|max:255",
			"details_range" => "required|string|max:255",
			"details_rules" => "required|string|max:255",
			"image_download" => "required|string|max:2",
			"image_attribute" => "required|string|max:10",
		];
	}

	protected function _model(AntsCollectTask $model)
	{
		$model->title = $this->input("title");
		$model->address = $this->input("address");
		$model->paging_address = $this->input("paging_address");
		$model->remove_head = $this->input("remove_head", '1') == 1 ? 1: 2;
		$model->list_range = $this->input("list_range");
		$model->list_rules = $this->input("list_rules");
		$model->details_range = $this->input("details_range");
		$model->details_rules = $this->input("details_rules");
		$model->image_download = $this->input("image_download", '1') == 1 ? 1: 2;
		$model->image_attribute = $this->input("image_attribute", 'src');
		$model->keywords_replace = !empty($this->input("keywords_replace", '')) ? $this->input("keywords_replace", '') : '';
		return $model;
	}
}