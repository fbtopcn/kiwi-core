<?php

namespace KiwiCore\Http\Request\Admin\AntsCollectTask;

use KiwiCore\Model\AntsCollectTask;
use KiwiCore\Http\Request\Request;

class UpdateCollectRequest extends Request
{
	use RulesCollect;

	public function model(AntsCollectTask $model)
	{
		return $this->_model($model);
	}
}