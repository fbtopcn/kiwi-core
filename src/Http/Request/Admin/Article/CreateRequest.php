<?php

namespace KiwiCore\Http\Request\Admin\Article;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\Article;
use KiwiCore\Model\ArticleVideo;

class CreateRequest extends Request
{
	use Rule {
		rules as _rules;
	}

	public function rules()
	{
		$rules = $this->_rules();
		return $rules;
	}

	public function model()
	{
		return $this->_model(new Article());
	}
}