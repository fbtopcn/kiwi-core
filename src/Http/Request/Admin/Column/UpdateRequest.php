<?php

namespace KiwiCore\Http\Request\Admin\Column;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\Column;

class UpdateRequest extends Request
{
	use Rules;

	public function model(Column $model)
	{
		return $this->_model($model);
	}
}