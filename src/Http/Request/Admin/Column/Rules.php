<?php

namespace KiwiCore\Http\Request\Admin\Column;

use KiwiCore\Model\Column;

trait Rules
{
	public function rules()
	{
		return [
			"title" => "required|string|max:20",
		];
	}

	protected function _model(Column $model)
	{
		$model->title = $this->input("title");
		return $model;
	}
}