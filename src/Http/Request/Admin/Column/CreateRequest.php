<?php

namespace KiwiCore\Http\Request\Admin\Column;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\Column;

class CreateRequest extends Request
{
	use Rules;

	public function model()
	{
		return $this->_model();
	}
}