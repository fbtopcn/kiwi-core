<?php

namespace KiwiCore\Http\Request\Admin\Tag;

use KiwiCore\Http\Request\Request;
use KiwiCore\Model\Tag;

class CreateRequest extends Request
{
	use Rules;

	public function model()
	{
		return $this->_model(new Tag());
	}
}