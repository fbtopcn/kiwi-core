<?php

namespace KiwiCore\Http\Request\Admin\Tag;

use KiwiCore\Model\Tag;
use KiwiCore\Http\Request\Request;

class UpdateRequest extends Request
{
	use Rules;

	public function model(Tag $model)
	{
		return $this->_model($model);
	}
}