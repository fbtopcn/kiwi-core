<?php

namespace KiwiCore\Http\Controllers\Traits;

use KiwiCore\Model\Element;
use KiwiCore\Repository\ArticleRepository;
use KiwiCore\Repository\ChannelRepository;
use KiwiCore\Repository\ElementRepository;
use KiwiCore\Service\LinkFormat;
use Illuminate\Support\Collection;

trait ArticleBlock
{
	/**
	 * @param string $elementPrefix
	 * @param string $channelName
	 * @return Collection
	 */
	public function buildParentBlocks($elementPrefix, $channelName)
	{
		$channelRepository = app(ChannelRepository::class);
		$channel = $channelRepository->findByName($channelName);
		if (empty($channel)) {
			return collect([]);
		}
		return $this->buildBlocks($elementPrefix, $channel->parent);
	}

	/**
	 * @param $elementPrefix
	 * @param $channelName
	 * @return Collection
	 */
	public function buildBlocks($elementPrefix, $channelName)
	{
		$blocks = $this->buildBlocksByElementParent($elementPrefix . $channelName);
		if ($blocks->isEmpty()) {
			return $this->buildBlocksByChannel($channelName);
		}
		return $blocks;
	}

	/**
	 * @param string $parentName
	 * @return Collection
	 */
	public function buildBlocksByElementParent($parentName)
	{
		$link = app(LinkFormat::class);
		$articleRepository = app(ArticleRepository::class);
		$elementRepository = app(ElementRepository::class);

		$blocks = $elementRepository->findByParent($parentName);
		if ($blocks->isEmpty()) {
			return collect([]);
		}

		foreach ($blocks as &$block) {

			// link
			$block->link = $link->full($block->link);

			$articles = collect([]);

			$attr = $block->attribute;

			// by ids, top
			if (isset($attr->ids)) {
				$articles = $articles->merge(
					$articleRepository->findByIds($attr->ids)
				);
			}

			if (isset($attr->count)) {
				$count = $attr->count - $articles->count();
				$content = isset($attr->content) ? $attr->content : false;

				if (isset($attr->channel) && isset($attr->tags)) {

					// channel and tags
					$articles = $articles->merge(
						$articleRepository->findByChannelFamilyAndTags(
							$attr->channel,
							$attr->tags,
							0,
							$count,
							$content)
					);
				} else if (isset($attr->channel)) {

					// channel
					$articles = $articles->merge(
						$articleRepository->findByChannelFamily(
							$attr->channel,
							0,
							$count,
							$content)
					);
				} else if (isset($attr->tags)) {

					// tags
					$articles = $articles->merge(
						$articleRepository->findByTags(
							$attr->tags,
							0,
							$count,
							$content)
					);
				}
			}
			$block->articles = $articles;
		}
		return $blocks;
	}

	/**
	 * @param string $channel
	 * @return Collection
	 */
	public function buildBlocksByChannel($channel)
	{
		$link = app(LinkFormat::class);
		$articleRepository = app(ArticleRepository::class);
		$channelRepository = app(ChannelRepository::class);

		$children = $channelRepository->findByParent($channel);
		$blocks = $children->map(function ($item) use ($articleRepository, $link) {
			$block = Element::createEmpty();
			$block->name = $item->name;
			$block->title = $item->title;
			$block->link = $this->channelBlockLink($item->name);
			$block->articles = $articleRepository->findByChannelFamily(
				$item->name,
				0,
				7,
				true);
			return $block;
		});
		return $blocks;
	}

	/**
	 * @param string $channelName
	 * @return string
	 */
	protected function channelBlockLink($channelName)
	{
		if (empty($channelName)) {
			return "";
		}

		$link = app(LinkFormat::class);
		$linkFactory = app("linkFactory");

		$blockMore = config("enle.block_more");
		if ($blockMore === "topic") {
			return $link->full($linkFactory->topic($channelName));
		} else {
			return $link->full($linkFactory->listChannel($channelName));
		}

		// no child, list link; else topic link
//			if ($this->channelRepository->findByParent($item->name)->isEmpty()) {
//				if (empty($this->topicTypeRepository->findByName($item->name))) {
//					$channel->link = $this->link->full($this->linkConverter->listChannelLink($item->name));
//				} else {
//					$channel->link = $this->link->full($this->linkConverter->topicLink($item->name));;
//				}
//			} else {
//				$channel->link = $this->link->full($this->linkConverter->topicLink($item->name));;
//			}
	}
}