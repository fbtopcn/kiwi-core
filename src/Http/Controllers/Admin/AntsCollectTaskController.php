<?php

namespace KiwiCore\Http\Controllers\Admin;


use KiwiCore\Condition\AntsCollectDataCondition;
use KiwiCore\Condition\AntsCollectTaskCondition;
use KiwiCore\Model\AntsCollectTask;
use KiwiCore\Repository\AntsCollectDataRepository;
use KiwiCore\Repository\AntsCollectTaskRepository;
use KiwiCore\Http\Request\Admin\AntsCollectTask\UpdatePublishedRequest;
use KiwiCore\Http\Request\Admin\AntsCollectTask\UpdateCollectRequest;
use KiwiCore\Http\Request\Admin\AntsCollectTask\UpdateRequest;
use KiwiCore\Service\AntsCollectService;
use Illuminate\Http\Request;

class AntsCollectTaskController extends AntsBaseController
{
	protected $repository;

	protected $antsCollectDataRepository;

	public function __construct()
	{
		parent::__construct();
		$this->repository = app(AntsCollectTaskRepository::class);
		$this->antsCollectDataRepository = app(AntsCollectDataRepository::class);
	}

    public function view(){

        $tasks = $this->repository->findByCondition(
            AntsCollectTaskCondition::all(),
            null,
            null,
            AntsCollectTaskCondition::sortByKeyAsc('state')
        );

        return $this->render('ants-task.list', compact('tasks'));
    }

    public function help(){

        return $this->render('ants-task.help');
    }

    public function task($id){
        $task = $this->repository->findById($id);

        return $this->render('ants-task.task', compact('task'));
    }

    public function copy(){
        $id = request()->input('taskId');
        $task = $this->repository->findById($id);

        $model = new AntsCollectTask();
        $model->title = $task->title.'_copy';
        $model->address = $task->address;
        $model->paging_address = $task->paging_address;
        $model->remove_head = $task->remove_head;
        $model->list_range = $task->list_range;
        $model->list_rules = $task->list_rules;
        $model->details_range = $task->details_range;
        $model->details_rules = $task->details_rules;
        $model->image_download = $task->image_download;
        $model->image_attribute = $task->image_attribute;
        $model->keywords_replace = $task->keywords_replace;
        $model->auto_published_rules = '';
        $this->repository->create($model);
        return $this->renderApi(self::returnAntsApi(self::TASK_SUCCESS, $model, '已镜像成功!'));
    }

    public function info($id){
	    $this->checkId($id);
        $info = $this->repository->findById($id);

        return $this->renderApi(self::returnAntsApi(self::TASK_SUCCESS, $info));
    }

	public function statistical($id){
	    $this->checkId($id);
        $statistical = $this->antsCollectDataRepository->findByCondition(
            AntsCollectDataCondition::statistical($id)
        )->keyBy('state');

        return $this->renderApi(self::returnAntsApi(self::TASK_SUCCESS, $statistical));
    }

    public function update(UpdateRequest $request, $id){
        $model = $this->repository->findById($id);
        $model = $request->model($model);
        $this->repository->update($model);

        return $this->renderApi(self::returnAntsApi(self::TASK_SUCCESS, $model));
    }

    public function updateTask(Request $request, $id){
        $model = $this->repository->findById($id);
        $model = $this->updateModel($model, $request);
        $this->repository->update($model);

        return $this->renderApi(self::returnAntsApi(self::TASK_SUCCESS, $model));
    }

    public function debug(Request $request){
        $debug_address = $request->input('debug_address');
        $debug_remove_head = $request->input('debug_remove_head');
        $debug_range = $request->input('debug_range');
        $debug_rules = $request->input('debug_rules');

        $result = app(AntsCollectService::class)->debug($debug_address, $debug_range, $debug_rules, $debug_remove_head);

        return $this->renderApi(self::returnAntsApi(self::TASK_SUCCESS, $result));
    }

    public function taskUpdatePublished($id, UpdatePublishedRequest $request){
        $model = $this->repository->findById($id);
        $model = $request->modelPublished($model);

        $this->repository->update($model);

        return $this->renderApi(self::returnAntsApi(self::TASK_SUCCESS, $model, '信息已保存成功!'));
    }

    public function taskUpdateCollect($id, UpdateCollectRequest $request){
        $model = $this->repository->findById($id);
        $model = $request->model($model);

        $this->repository->update($model);

        return $this->renderApi(self::returnAntsApi(self::TASK_SUCCESS, $model, '信息已保存成功!'));
    }

    public function create(Request $request)
    {
        $model = new AntsCollectTask();
        $model->title = $request->input('title');
        $model->keywords_replace = '';
        $model->auto_published_rules = '';
        $this->repository->create($model);
        return $this->renderApi(self::returnAntsApi(self::TASK_SUCCESS, $model, '已创建成功!'));
    }

    public function startJob($id){
        $model = $this->repository->findById($id);

        $result = app(AntsCollectService::class)->startListJob($model);

        return $this->renderApi($result);
    }

    public function startJobPaging($id){
        $model = $this->repository->findById($id);
        $pageNumber = request()->input('pageNumber', '2');

        $result = app(AntsCollectService::class)->startPagingJob($model, $pageNumber);

        return $this->renderApi($result);
    }

    public function startJobDetails($id){
        $model = $this->repository->findById($id);
        $stepLength = request()->input('stepLength', '5');

        $result = app(AntsCollectService::class)->startJobDetails($model, $stepLength);

        return $this->renderApi($result);
    }

}