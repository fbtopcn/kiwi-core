<?php

namespace KiwiCore\Http\Controllers\Admin;


use KiwiCore\Condition\AliasLinkCondition;
use KiwiCore\Http\Controllers\AdminController;
use KiwiCore\Repository\AliasLinkRepository;
use KiwiCore\Http\Request\Admin\AliasLink\CreateRequest;
use KiwiCore\Http\Request\Admin\AliasLink\UpdateRequest;

class AliasLinkController extends AdminController
{
    use TableTrait;

    protected static $columns = [
        ["name" => "id", "type" => "id", "title" => "ID", "data" => "id"],
        ["name" => "__op", "type" => "op", "title" => "操作", "data" => "__op", "target" => [
            ["name" => "update", "title" => "编辑"],
            ["name" => "remove", "title" => "删除"],
        ]],
        ["name" => "global", "type" => "enum", "title" => "全局替换", "data" => "global", "target" => [
            0 => "否",
            1 => "是",
        ]],
        ["name" => "alias", "type" => "string", "title" => "别名", "data" => "alias"],
        ["name" => "link", "type" => "string", "title" => "内部链接", "data" => "link"],
    ];

    protected static $filters = [
        ["name" => "alias"],
        ["name" => "link"],
    ];

    protected $repository;

    public function __construct(AliasLinkRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->modelName = "AliasLink";
    }

    protected function repository()
    {
        return $this->repository;
    }

    protected function listByFilter($start, $length, array $filters = [])
    {
        $alias = $filters["alias"];
        $link = $filters["link"];
        $filter = new \stdClass();
        if (!empty($alias)) {
            $filter->alias = $alias;
        }
        if (!empty($link)) {
            $filter->link = $link;
        }

        $friendLinks = $this->repository->findByCondition(
            AliasLinkCondition::byFilter($filter),
            null,
            AliasLinkCondition::pagerBySequence($start, $length),
            AliasLinkCondition::sortById()
        );
        return [$friendLinks, $this->repository->countByCondition(AliasLinkCondition::byFilter($filter))];
    }

    public function create(CreateRequest $request)
    {
        $model = $request->model();
        $this->repository()->create($model);
        return $this->renderApi($model);
    }

    public function update(UpdateRequest $request, $id)
    {
        $model = $this->checkModelById($id);
        $model = $request->model($model);
        $this->repository()->update($model);
        return $this->renderApi($model);
    }

    public function delete($id)
    {
        $id = $this->checkId($id);
        $this->repository()->delete($id);
        return $this->renderApi();
    }
}