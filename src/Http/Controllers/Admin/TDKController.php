<?php

namespace KiwiCore\Http\Controllers\Admin;


use KiwiCore\Condition\TdkCondition;
use KiwiCore\Http\Controllers\AdminController;
use KiwiCore\Http\Request\Admin\TDK\UpdateRequest;
use KiwiCore\Repository\TdkRepository;
use KiwiCore\Foundation\Environment;
use KiwiCore\Http\Request\Admin\TDK\CreateRequest;
use KiwiCore\Service\CheckById;

class TDKController extends AdminController
{
    use TableTrait, CheckById;

    protected static $columns = [
        ["name" => "id", "type" => "id", "title" => "ID", "data" => "id"],
        ["name" => "__op", "type" => "op", "title" => "操作", "data" => "__op", "target" => [
            ["name" => "update", "title" => "编辑"],
            ["name" => "remove", "title" => "删除"],
        ]],
        ["name" => "pattern", "type" => "string", "title" => "URL", "data" => "pattern"],
        ["name" => "domain", "type" => "enum", "title" => "站点", "data" => "domain", "target" => [
            Environment::DOMAIN_PC => "PC",
            Environment::DOMAIN_MOBILE => "移动",
        ]],
        ["name" => "title", "type" => "string", "title" => "Title", "data" => "title"],
        ["name" => "keywords", "type" => "string", "title" => "Keywords", "data" => "keywords"],
        ["name" => "description", "type" => "string", "title" => "Description", "data" => "description"],
    ];

    protected static $filters = [
        ["name" => "pattern"],
        ["name" => "domain"]
    ];

    protected $repository;

    public function __construct(TdkRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->modelName = "TDK";
    }

    protected function repository()
    {
        return $this->repository;
    }

    protected function listByFilter($start, $length, array $filters = [])
    {
        $pattern = $filters["pattern"];
        $domain = $filters["domain"];
        $filter = new \stdClass();
        if (!empty($pattern)) {
            $filter->pattern = $pattern;
        }
        if (!empty($domain)) {
            $filter->domain = $domain;
        }

        $tdks = $this->repository->findByCondition(
            TdkCondition::byFilter($filter),
            null,
            TdkCondition::pagerBySequence($start, $length),
            TdkCondition::sortById()
        );

        $count = $this->repository->countByCondition(TdkCondition::byFilter($filter));

        return [$tdks, $count];
    }

    public function create(CreateRequest $request)
    {
        $model = $request->model();
        // TODO platform & pattern unique

        $this->repository()->create($model);
        return $this->renderApi($model);
    }

    public function update(UpdateRequest $request, $id)
    {
        $model = $this->checkModelById($id);
        $model = $request->model($model);
        // TODO platform & pattern unique

        $this->repository()->update($model);
        return $this->renderApi($model);
    }

    public function delete($id)
    {
        $id = $this->checkId($id);
        $this->checkTdk($id);
        $this->repository()->delete($id);
        return $this->renderApi();
    }
}