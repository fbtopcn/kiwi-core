<?php

namespace KiwiCore\Http\Controllers\Admin;


use KiwiCore\Condition\ChannelCondition;
use KiwiCore\Http\Controllers\AdminController;
use KiwiCore\Repository\ArticleChannelRepository;
use KiwiCore\Repository\ArticleRepository;
use KiwiCore\Repository\ChannelRepository;
use KiwiCore\Http\Request\Admin\Channel\CreateRequest;
use KiwiCore\Http\Request\Admin\Channel\UpdateRequest;
use KiwiCore\Service\Admin\TreeView;
use KiwiCore\Service\CheckById;

class ChannelController extends AdminController
{
	use TableTrait, CheckById;

    protected static $columns = [
        ["name" => "id", "type" => "id", "title" => "ID", "data" => "id"],
        ["name" => "__op", "type" => "op", "title" => "操作", "data" => "__op", "target" => [
            ["name" => "update", "title" => "编辑"],
            ["name" => "remove", "title" => "删除"],
        ]],
        ["name" => "name", "type" => "string", "title" => "英文名称", "data" => "name"],
        ["name" => "title", "type" => "string", "title" => "中文名称", "data" => "title"],
        ["name" => "parent", "type" => "string", "title" => "父频道", "data" => "parent"],
        ["name" => "position", "type" => "number", "title" => "位置", "data" => "position"],
    ];

    protected static $filters = [
        ["name" => "title"],
    ];

    protected $repository;

    protected $articleChannelRepository;

    protected $articleRepository;

    public function __construct(
        ChannelRepository $repository,
        ArticleChannelRepository $articleChannelRepository,
        ArticleRepository $articleRepository
    )
    {
        parent::__construct();
        $this->repository = $repository;
        $this->articleChannelRepository = $articleChannelRepository;
        $this->articleRepository = $articleRepository;
        $this->modelName = "Channel";
    }

    protected function repository()
    {
        return $this->repository;
    }

    protected function listByFilter($start, $length, array $filters = [])
    {
        $title = $filters["title"];

        $filter = new \stdClass();
        if (!empty($title)) {
            $filter->title = $title;
        }

        $condition = ChannelCondition::byFilter($filter);
        $channels = $this->repository->findByCondition(
            $condition,
            null,
            ChannelCondition::pagerBySequence($start, $length),
            ChannelCondition::sortById()
        );

        $count = $this->repository->countByCondition($condition);
        return [$channels, $count, null];
    }

    public function create(CreateRequest $request)
    {
        $channel = $request->model();

        $this->repository->create($channel);
        return $this->renderApi($channel);
    }

    /**
     * @param UpdateRequest $request
     * @param $id
     * @return array
     * @throws \KiwiCore\Exceptions\NotFoundException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(UpdateRequest $request, $id)
    {
        $this->checkId($id);
        $channel = $this->checkChannel($id);
        $channelName = $channel->name;
        $channel = $request->model($channel);

        $this->repository->update($channel);
        $this->relyChange($channelName, $channel->name);
        return $this->renderApi($channel);
    }

    /**
     * @param $id
     * @return array
     * @throws \KiwiCore\Exceptions\NotFoundException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function delete($id)
    {
        $this->checkId($id);
        $channel = $this->checkChannel($id);

        $this->repository()->delete($id);
        $this->articleChannelRepository->query()->where(['channel' => $channel->name])->delete();
        return $this->renderApi();
    }

    /**
     * @param TreeView $treeView
     * @return array
     */
    public function tree(TreeView $treeView)
    {
        $tree = $treeView->build($this->repository->all());
        return $this->renderApi($tree);
    }

    /**
     * 依赖变更
     * @param $channelNameOrigin
     * @param $channelName
     * @return int|void
     */
    public function relyChange($channelNameOrigin, $channelName){
        if ($channelNameOrigin === $channelName){
            return;
        }

        $this->articleRepository->query()->where(['channel' => $channelNameOrigin])->update(['channel' => $channelName]);
        return $this->articleChannelRepository->query()->where(['channel' => $channelNameOrigin])->update(['channel' => $channelName]);
    }

}