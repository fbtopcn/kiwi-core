<?php

namespace KiwiCore\Http\Controllers\Admin;


use KiwiCore\Condition\FriendLinkCondition;
use KiwiCore\Http\Controllers\AdminController;
use KiwiCore\Repository\FriendLinkRepository;
use KiwiCore\Foundation\Environment;
use KiwiCore\Http\Request\Admin\FriendLink\CreateRequest;
use KiwiCore\Http\Request\Admin\FriendLink\UpdateRequest;

class FriendLinkController extends AdminController
{
	use TableTrait;

	protected static $columns = [
		["name" => "id", "type" => "id", "title" => "ID", "data" => "id"],
		["name" => "__op", "type" => "op", "title" => "操作", "data" => "__op", "target" => [
			["name" => "update", "title" => "编辑"],
			["name" => "remove", "title" => "删除"],
		]],
		["name" => "title", "type" => "string", "title" => "文字", "data" => "title"],
		["name" => "link", "type" => "string", "title" => "链接", "data" => "link"],
		["name" => "position", "type" => "number", "title" => "位置", "data" => "position"],
		["name" => "pattern", "type" => "string", "title" => "URL", "data" => "pattern"],
		["name" => "domain", "type" => "enum", "title" => "站点", "data" => "domain", "target" => [
			Environment::DOMAIN_PC => "PC",
			Environment::DOMAIN_MOBILE => "移动",
		]],
	];

	protected static $filters = [
		["name" => "pattern"],
		["name" => "domain"]
	];

	protected $repository;

	public function __construct(FriendLinkRepository $repository)
	{
		parent::__construct();
		$this->repository = $repository;
		$this->modelName = "FriendLink";
	}

	protected function repository()
	{
		return $this->repository;
	}

	protected function listByFilter($start, $length, array $filters = [])
	{
        $pattern = $filters["pattern"];
        $domain = $filters["domain"];
        $filter = new \stdClass();
        if (!empty($pattern)) {
            $filter->pattern = $pattern;
        }
        if (!empty($domain)) {
            $filter->domain = $domain;
        }

		$friendLinks = $this->repository->findByCondition(
            FriendLinkCondition::byFilter($filter),
            null,
            FriendLinkCondition::pagerBySequence($start, $length),
            FriendLinkCondition::sortById()
        );

		$count = $this->repository->countByCondition(FriendLinkCondition::byFilter($filter));
		return [$friendLinks, $count];
	}

    public function create(CreateRequest $request)
    {
        $model = $request->model();
        $this->repository()->create($model);
        return $this->renderApi($model);
    }

    public function update(UpdateRequest $request, $id)
    {
        $model = $this->checkModelById($id);
        $model = $request->model($model);
        $this->repository()->update($model);
        return $this->renderApi($model);
    }

    public function delete($id)
    {
        $id = $this->checkId($id);
        $this->repository()->delete($id);
        return $this->renderApi();
    }
}