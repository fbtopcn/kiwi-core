<?php

namespace KiwiCore\Http\Controllers\Admin;

use Illuminate\Support\Facades\Log;
use KiwiCore\Condition\AntsCollectPublishedCondition;
use KiwiCore\Http\Request\Admin\AntsCollectPublished\CreateRequest;
use KiwiCore\Http\Request\Admin\AntsCollectPublished\UpdateRequest;
use KiwiCore\Repository\AntsCollectPublishedRepository;
use KiwiCore\Repository\AntsCollectTaskRepository;
use KiwiCore\Service\AntsCollectService;

class AntsCollectPublishedController extends AntsBaseController
{
	protected $repository;

	protected $antsCollectTaskRepository;

	public function __construct()
	{
		parent::__construct();
		$this->repository = app(AntsCollectPublishedRepository::class);
        $this->antsCollectTaskRepository = app(AntsCollectTaskRepository::class);
	}

	public function view(){
        $scheduling = $this->repository->findByCondition(
            AntsCollectPublishedCondition::all()
        );

        return $this->render('ants-task.scheduling', compact('scheduling'));
    }

    public function release($id){
        $task = $this->antsCollectTaskRepository->findById($id);

        return $this->render('ants-task.release', compact('task'));
    }

    public function create(CreateRequest $request){
        $model = $request->model();

        $this->repository->create($model);
        return $this->renderApi(AntsBaseController::returnAntsApi(AntsBaseController::TASK_SUCCESS, $model));
    }

    public function info($id){
        $this->checkId($id);
        $model = $this->repository->findById($id);
        return $this->renderApi(AntsBaseController::returnAntsApi(AntsBaseController::TASK_SUCCESS, $model));
    }

    public function delete($id){
        $this->checkId($id);
        $this->repository->delete($id);
        return $this->renderApi(AntsBaseController::returnAntsApi(AntsBaseController::TASK_SUCCESS));
    }

    public function update(UpdateRequest $request, $id){

        $this->checkId($id);
        $model = $this->repository->findById($id);
        $model = $request->model($model);

        $this->repository->update($model);
        return $this->renderApi(AntsBaseController::returnAntsApi(AntsBaseController::TASK_SUCCESS));
    }

    public function push($id){
        $rules = request()->input('rules');
        $rules = json_decode($rules);
        if (!$rules){
            return $this->renderApi(self::returnAntsApi(self::TASK_FAILURE, [], '规则错误.'));
        }

        Log::info('Manual Published Start. TaskID: '.$id);
        app(AntsCollectService::class)->autoPublishedData([$id], $rules);
        Log::info('Manual Published Stop. TaskID: '.$id);

        return $this->renderApi(self::returnAntsApi(self::TASK_SUCCESS));
    }

}