<?php

namespace KiwiCore\Http\Controllers\Admin;


use KiwiCore\Http\Controllers\AdminController;
use KiwiCore\Model\AdminLog;
use KiwiCore\Service\Admin\DataTable;
use KiwiCore\Service\Admin\Pager;
use Illuminate\Http\Request;

class LogController extends AdminController
{
	protected static $columns = [
		["name" => "user", "type" => "string", "title" => "user", "data" => "user"],
		["name" => "createdAt", "type" => "datetime", "title" => "createdAt", "data" => "createdAt"],
		["name" => "action", "type" => "string", "title" => "action", "data" => "action"],
		["name" => "value", "type" => "string", "title" => "value", "data" => "value"],
	];

	protected static $filters = [
		["name" => "user"],
		["name" => "createdAt"],
	];

	public function __construct()
	{
		parent::__construct();
	}

	public function view()
	{
		return $this->render("log.list", [
			"staticPath" => "",
			"columns" => json_encode(static::$columns),
			"filters" => json_encode(static::$filters),
		]);
	}

	public function index(Request $request, DataTable $dataTable)
	{
		list($start, $length) = app(Pager::class)->get($request);
		$filters = $dataTable->getFilter(
			$request->all(),
			static::$columns,
			static::$filters);

		// TODO filter created_at
		$query = AdminLog::query();
		if ($filters["user"]) {
			$query = $query->where("user", $filters["user"]);
		}

		$logs = $query->orderBy("created_at", "desc")
			->skip($start)->take($length)->get();

		$query = AdminLog::query();
		if ($filters["user"]) {
			$query = $query->where("user", $filters["user"]);
		}
		$length = $query->count();

		return [
			"draw" => $request->draw,
			"recordsTotal" => $length,
			"recordsFiltered" => $length,
			"data" => $logs,
		];
	}
}