<?php

namespace KiwiCore\Http\Controllers\Admin;


use KiwiCore\Exceptions\NotFoundException;
use KiwiCore\Http\Request\Admin\ListRequest;
use KiwiCore\Repository\BaseRepository;
use KiwiCore\Repository\ManualDataRepository;
use KiwiCore\Service\Admin\DataTable;
use KiwiCore\Service\CheckId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;

trait TableTrait
{
    use CheckId;

    /**
     * @return BaseRepository
     */
    abstract protected function repository();

    /**
     * @param int $start
     * @param int $length
     * @param array $filters
     * @return array (Collection, count)
     */
    abstract protected function listByFilter($start, $length, array $filters = []);

    /**
     * @return \Closure
     */
    protected function createOpFilter()
    {
        return function () {
            return true;
        };
    }

    /**
     * @return array
     */
    protected function listViewData()
    {
        return [];
    }

    /**
     * @param Collection $models
     */
    protected function listAppend(Collection &$models)
    {
    }

    /**
     * @param Model $model
     */
    protected function infoAppend(Model &$model)
    {
    }

    /**
     * @var string
     */
    protected $modelName;

    public function view()
    {
        $data = array_merge($this->listViewData(), [
            "columns" => json_encode(static::$columns),
            "filters" => json_encode(static::$filters),
        ]);

        $name = strtolower($this->modelName);
        return $this->render("$name.list", $data);
    }

    public function index(ListRequest $request)
    {
        $dataTable = app(DataTable::class);
        $filters = $dataTable->getFilter(
            $request->input(),
            static::$columns,
            static::$filters);

        if (isset($filters["id"]) && !empty($filters["id"])) {
            $model = $this->repository()->findById($filters["id"]);
            if (empty($model)) {
                $count = 0;
                $models = collect([]);
            } else {
                $count = 1;
                $models = collect([$model]);
                $this->listAppend($models);
            }
        } else {
            list($models, $count) = $this->listByFilter(
                $request->start(),
                $request->length(),
                $filters);
            $this->listAppend($models);
        }

        $opFilter = $this->createOpFilter();

        return [
            "draw" => $request->draw,
            "recordsTotal" => $count,
            "recordsFiltered" => $count,
            "data" => $dataTable->build($models, static::$columns, $opFilter)
        ];
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundException
     * @throws ValidationException
     */
    public function info($id)
    {
        $model = $this->checkModelById($id);
        $this->infoAppend($model);
        return $this->renderApi($model);
    }

    /**
     * @param $id
     * @return Model
     * @throws NotFoundException
     * @throws ValidationException
     */
    protected function checkModelById($id)
    {
        $id = $this->checkId($id);
        $model = $this->repository()->findById($id);
        if (empty($model)) {
            throw new NotFoundException("$this->modelName $id");
        }
        return $model;
    }
}