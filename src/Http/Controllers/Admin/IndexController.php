<?php
namespace KiwiCore\Http\Controllers\Admin;

use KiwiCore\Http\Controllers\AdminController;

class IndexController extends AdminController{
    public function index(){

        return $this->render('index.index');
    }
}