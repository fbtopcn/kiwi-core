<?php

namespace KiwiCore\Exceptions;

use Exception;

class NotFoundException extends Exception
{
	public function renderJson()
	{
		return [
			"status" => 404,
			"message" => "Not Found " . $this->getMessage(),
		];
	}
}