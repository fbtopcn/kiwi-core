<?php
namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;
use KiwiCore\Condition\Pager;
use KiwiCore\Condition\Selector;
use KiwiCore\Condition\Sorter;

class AntsCollectPublishedCondition
{
    use Sorter, Pager, Selector;

    public static function all()
    {
        return function (Builder $query) {
            return $query;
        };
    }

    public static function state($state)
    {
        return function (Builder $query) use ($state){
            return $query->where('state', $state);
        };
    }


}