<?php

namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

trait Pager
{
	public static function pagerById($cursor, $count)
	{
		return function (Builder $query) use ($cursor, $count) {
			if ($cursor !== 0) {
				$query = $query->where("id", "<", $cursor);
			}
			return $query->take($count);
		};
	}

	public static function pagerBySequence($start, $length)
	{
		return function (Builder $query) use ($start, $length) {
			return $query
				->skip($start)
				->take($length);
		};
	}
}