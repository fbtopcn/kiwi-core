<?php
namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

class VariableCondition
{
    use Sorter, Pager, Selector;

    public static function all()
    {
        return function (Builder $query){
            return $query;
        };
    }

    public static function name($name)
    {
        return function (Builder $query) use ($name){
            return $query->where('name', $name);
        };
    }

}