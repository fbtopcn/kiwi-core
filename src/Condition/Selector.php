<?php

namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

trait Selector
{
    public static function selectId()
    {
        return function (Builder $query) {
            return $query->select("id");
        };
    }
}