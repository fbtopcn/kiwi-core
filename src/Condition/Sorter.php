<?php

namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

trait Sorter
{
	public static function sortById()
	{
		return function (Builder $query) {
			return $query->orderBy("id", "desc");
		};
	}

	public static function sortByIdAsc()
	{
		return function (Builder $query) {
			return $query->orderBy("id", "asc");
		};
	}
	public static function sortByKey($key)
	{
		return function (Builder $query) use ($key) {
			return $query->orderBy($key, "desc");
		};
	}

	public static function sortByKeyAsc($key)
	{
		return function (Builder $query) use ($key) {
			return $query->orderBy($key, "asc");
		};
	}
}