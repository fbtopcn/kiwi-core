<?php
namespace KiwiCore\Condition;

use KiwiCore\Model\Article;
use Illuminate\Database\Eloquent\Builder;

class ArticleCondition
{
    use Sorter, Pager, Selector;

    public static function all()
    {
        return function (Builder $query) {
            return $query->where('state', Article::STATE_PUBLISHED);
        };
    }

    public static function byIds(array $ids)
    {
        return function (Builder $query) use ($ids) {
            $query->where('state', Article::STATE_PUBLISHED);

            return $query->whereIn('id', $ids);
        };
    }

    public static function channel($channel)
    {
        return function (Builder $query) use ($channel) {
            $query->where('state', Article::STATE_PUBLISHED);

            return $query->where('channel', $channel);
        };
    }

    public static function channelPaging($channel)
    {
        return function (Builder $query) use ($channel) {
            $query->where('state', Article::STATE_PUBLISHED);

            return $query->where('channel', $channel);
        };
    }

    public static function search($keyword)
    {
        return function (Builder $query) use ($keyword) {
            $query->where('state', Article::STATE_PUBLISHED);

            return $query->where('title', 'like', "%$keyword%");
        };
    }

    public static function byFilter($filter){
        return function (Builder $query) use ($filter) {
            if (isset($filter->title)){
                $query->where('title', 'like', "%$filter->title%");
            }
            if (isset($filter->content)){
                $query->where('content', 'like', "%$filter->content%");
            }
            return $query;
        };
    }


}