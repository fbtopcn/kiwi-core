<?php
namespace KiwiCore\Condition;

use Illuminate\Database\Eloquent\Builder;

class ChannelCondition
{
    use Sorter, Pager, Selector;

    /**
     * all
     * @return \Closure
     */
    public static function all()
    {
        return function (Builder $query) {
            return $query;
        };
    }

    /**
     * @param $channel
     * @return \Closure
     */
    public static function channel($channel)
    {
        return function (Builder $query) use ($channel) {
            return $query->where('name', $channel);
        };
    }

    /**
     * @param $channels
     * @return \Closure
     */
    public static function channels($channels)
    {
        return function (Builder $query) use ($channels) {
            return $query->whereIn('name', $channels);
        };
    }

    /**
     * @param $filter
     * @return \Closure
     */
    public static function byFilter($filter){
        return function (Builder $query) use ($filter) {
            if (isset($filter->title)){
                $query->where('title', 'like', "%$filter->title%");
            }
            return $query;
        };
    }
}