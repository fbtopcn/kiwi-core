<?php
namespace KiwiCore\Utils;

use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;

class BeautifyUrlLengthAwarePaginator extends LengthAwarePaginator
{

    public static function injectIntoBuilder()
    {
        Builder::macro('beautifyUrlPaginator', function ($perPage, $columns, $pageName, $page) {
            $perPage = $perPage ?: $this->model->getPerPage();

            $items = ($total = $this->toBase()->getCountForPagination())
                ? $this->forPage($page, $perPage)->get($columns)
                : $this->model->newCollection();

            $options = [
                'path' => Paginator::resolveCurrentPath(),
                'pageName' => $pageName,
            ];

            return Container::getInstance()->makeWith(BeautifyUrlLengthAwarePaginator::class, compact(
                'items', 'total', 'perPage', 'page', 'options'
            ));
        });
    }


    /**
     * 重写页面 URL 实现代码，去掉分页中的问号，实现伪静态链接
     * @param int $page
     * @return string
     */
    public function url($page)
    {
        if ($page <= 0) {
            $page = 1;
        }
        $originPath = $this->path;

        // 移除路径尾部的/
        $path = rtrim($this->path, '/');

        // 如果路径中包含分页信息则正则替换页码，否则将页码信息追加到路径末尾
        if (preg_match('/\/page\/\d+/', $path)) {
            $path = preg_replace('/\/page\/\d+/', '/page/' . $page, $path);
        } elseif (preg_match('/([a-z]+|0+)_(\d+)_(\d+)_(\d+)_(\d+)_(\d+).html/', $path)) {
            $path = preg_replace('/([a-z]+|0+)_(\d+)_(\d+)_(\d+)_(\d+)_(\d+).html/', '${1}_${2}_${3}_${4}_${5}_'.$page.'.html', $path);
        } elseif (preg_match('/classify|all$/', $path, $section)) {
            $path = '/'.$section[0].'/0_0_0_0_1_'.$page.'.html';
        } else {
            $path .= '/page/' . $page;
        }

        $path .= config('kiwi.paging_format', false) === true ?'/':'';
        $this->path = $path;

        if ($this->query) {
            $url = $this->path . (Str::contains($this->path, '?') ? '&' : '?')
                . http_build_query($this->query, '', '&')
                . $this->buildFragment();
        } elseif ($this->fragment) {
            $url = $this->path . $this->buildFragment();
        } else {
            $url = $this->path;
        }
        // 不污染原始path
        $this->path = $originPath;

        return $url;
    }

    /**
     * 重写当前页设置方法
     *
     * @param  int  $currentPage
     * @param  string  $pageName
     * @return int
     */
    protected function setCurrentPage($currentPage, $pageName)
    {

        if (!$currentPage && preg_match('/\/page\/(\d+)/', $this->path, $matches)) {
            $currentPage = $matches[1];
        } elseif (!$currentPage && preg_match('/([a-z]+|0+)_(\d+)_(\d+)_(\d+)_(\d+)_(\d+).html/', $this->path, $matches)) {
            $currentPage = $matches[6];
        } elseif (!$currentPage && preg_match('/classify|all$/', $this->path, $matches)) {
            $currentPage = 1;
        }

        return $this->isValidPageNumber($currentPage) ? (int) $currentPage : 1;
    }

}