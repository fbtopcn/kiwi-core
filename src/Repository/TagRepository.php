<?php

namespace KiwiCore\Repository;

use KiwiCore\Model\Tag;
use KiwiCore\Repository\Base\CrudQuery;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class TagRepository extends BaseRepository
{
    use CrudQuery;

    protected function query()
    {
        return Tag::query();
    }

    public function article($id)
    {
        return DB::table("article_tag")->where("article_id", $id)->get();
    }

    public function findByNames(array $names)
    {
        return $this->query()->whereIn("name", $names)->get();
    }

    public function all(){
        return $this->query()->get();
    }

    public function simple(Collection $tags)
    {
        $result = [];
        foreach ($tags as $tag) {
            $item = new \stdClass();
            $item->id = $tag->id;
            $item->name = $tag->name;
            $item->title = $tag->title;
            $item->type = $tag->type;
            $result[] = $item;
        }
        return $result;
    }
}