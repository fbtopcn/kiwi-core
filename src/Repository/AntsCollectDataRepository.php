<?php

namespace KiwiCore\Repository;

use KiwiCore\Model\AntsCollectData;
use KiwiCore\Repository\Base\CrudQuery;
use KiwiCore\Repository\BaseRepository;

class AntsCollectDataRepository extends BaseRepository
{
    use CrudQuery;

    public function query()
    {
        return AntsCollectData::query();
    }

}