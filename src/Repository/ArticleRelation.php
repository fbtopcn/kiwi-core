<?php

namespace KiwiCore\Repository;


use Illuminate\Support\Collection;

trait ArticleRelation
{

	/**
	 * @param array $tags
	 * @param int $skip
	 * @param int $count
	 * @param bool $containContent
	 * @return Collection
	 */
	public function findByTags(array $tags, $skip = 0, $count = PHP_INT_MAX, $containContent = false)
	{
		return $this->findByChannelsAndTags([], $tags, $skip, $count, $containContent);
	}

	/**
	 * @param array $tags
	 * @return int
	 */
	public function countByTags(array $tags)
	{
		return $this->countByChannelsAndTags([], $tags);
	}

	/**
	 * @param string $channel
	 * @param int $skip
	 * @param int $count
	 * @param bool $containContent
	 * @return Collection
	 */
	public function findByChannelFamily($channel, $skip = 0, $count = PHP_INT_MAX, $containContent = false)
	{
		$channels = $this->channelRepository()->findFamilyByName($channel);
		return $this->findByChannelsAndTags($channels, [], $skip, $count, $containContent);
	}

	/**
	 * @param string $channel
	 * @return int
	 */
	public function countByChannelFamily($channel)
	{
		$channels = $this->channelRepository()->findFamilyByName($channel);
		return $this->countByChannelsAndTags($channels, []);
	}

	/**
	 * @param string $channel
	 * @param int $skip
	 * @param int $count
	 * @param bool $containContent
	 * @return Collection
	 */
	public function findByChannel($channel, $skip = 0, $count = PHP_INT_MAX, $containContent = false)
	{
		return $this->findByChannelsAndTags([$channel], [], $skip, $count, $containContent);
	}

	/**
	 * @param string $channel
	 * @return int
	 */
	public function countByChannel($channel)
	{
		return $this->countByChannelsAndTags([$channel], []);
	}

	/**
	 * @param string $channel
	 * @param array $tags
	 * @param int $skip
	 * @param int $count
	 * @param bool $containContent
	 * @return Collection
	 */
	public function findByChannelAndTags(
		$channel,
		$tags,
		$skip = 0,
		$count = PHP_INT_MAX,
		$containContent = false)
	{
		return $this->findByChannelsAndTags([$channel], $tags, $skip, $count, $containContent);
	}

	/**
	 * @param string $channel
	 * @param array $tags
	 * @return int
	 */
	public function countByChannelAndTags($channel, $tags)
	{
		return $this->countByChannelsAndTags([$channel], $tags);
	}

	/**
	 * @param string $channel
	 * @param array $tags
	 * @param int $skip
	 * @param int $count
	 * @param bool $containContent
	 * @return Collection
	 */
	public function findByChannelFamilyAndTags(
		$channel,
		$tags,
		$skip = 0,
		$count = PHP_INT_MAX,
		$containContent = false)
	{
		$channels = $this->channelRepository()->findFamilyByName($channel);
		$result = $this->findByChannelsAndTags($channels, $tags, $skip, $count, $containContent);
		return $result;
	}

	/**
	 * @param string $channel
	 * @param array $tags
	 * @return int
	 */
	public function countByChannelFamilyAndTags($channel, $tags)
	{
		$channels = $this->channelRepository()->findFamilyByName($channel);
		return $this->countByChannelsAndTags($channels, $tags);
	}

	/**
	 * @param array $channels
	 * @param array $tags
	 * @param int $skip
	 * @param int $count
	 * @param bool $containContent
	 * @return Collection
	 */
	protected function findByChannelsAndTags($channels, $tags, $skip, $count, $containContent)
	{
		return $this->findByRelation($skip, $count, $containContent, function () use ($channels, $tags) {
			return $this->relationFilter($channels, $tags);
		});
	}

	/**
	 * @param array $channels
	 * @param array $tags
	 * @return int
	 */
	protected function countByChannelsAndTags($channels, $tags)
	{
		return $this->countByRelation(function () use ($channels, $tags) {
			return $this->relationFilter($channels, $tags);
		});
	}

	/**
	 * return null, not filter; return [], no compatible article
	 * @param array $channels
	 * @param array $tags
	 * @return Collection | null
	 */
	protected function relationFilter($channels, $tags)
	{
		if (empty($channels)) {
			$idsChannel = null;
		} else {
			$idsChannel = $this->channelRepository()->inMatch($channels);
		}
		if (empty($tags)) {
			$idsTags = null;
		} else {
			$idsTags = $this->tagRepository()->extraMatch($tags);
		}

		if ($idsChannel === null && $idsTags === null) {
			return null;
		}
		if ($idsChannel === null) {
			return $idsTags;
		}
		if ($idsTags === null) {
			return $idsChannel;
		}
		return $idsChannel->intersect($idsTags->all())->values();
	}

	/**
	 * @param int $skip
	 * @param int $count
	 * @param bool $containContent
	 * @param \Closure $relationFilter
	 * @return Collection
	 */
	protected function findByRelation($skip, $count, $containContent, \Closure $relationFilter)
	{
		$select = ["id", "title", "short_title", "image", "published_at", "created_at", "updated_at"];
		if ($containContent === true) {
			$select[] = "content";
		}

		$query = $this->query()->select($select);
		$articleIds = $relationFilter($query);
		if ($articleIds !== null) {
			if (count($articleIds) === 0) {
				return collect([]);
			}
			$query = $query->whereIn("id", $articleIds);
		}
		return $this->decodeCollection($query->skip($skip)->take($count)->get());
	}

	/**
	 * @param \Closure $relationFilter
	 * @return int
	 */
	protected function countByRelation(\Closure $relationFilter)
	{
		$query = $this->query()->withoutGlobalScope("order");
		$articleIds = $relationFilter($query);
		if ($articleIds === null) {
			return $query->count();
		}
		if (count($articleIds) === 0) {
			return 0;
		}
		return $query->whereIn("id", $articleIds)->count();
	}

}