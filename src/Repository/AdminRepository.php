<?php

namespace KiwiCore\Repository;


use KiwiCore\Model\Admin;

class AdminRepository extends BaseRepository implements TableRepository
{
	public function create($admin)
	{
		if ($admin->save()) {
			return $admin->id;
		}
		return -1;
	}

	public function update($admin)
	{
		$admin->save();
	}

	public function delete($id)
	{
		Admin::destroy($id);
	}

	public function findById($id)
	{
		return Admin::query()->where("id", $id)->first();
	}

	public function find($start = 0, $count = PHP_INT_MAX)
	{
		return Admin::query()->skip($start)->take($count)->get();
	}

	public function count()
	{
		return Admin::query()->count();
	}

	public function findByName($name)
	{
		return Admin::query()->where("name", $name)->first();
	}
}