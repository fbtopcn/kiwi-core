<?php

namespace KiwiCore\Repository;


use Illuminate\Database\Eloquent\Model;

interface TableRepository
{
	/**
	 * @param int $id
	 * @return Model
	 */
	public function findById($id);

	/**
	 * @param Model $model
	 * @return int insert id
	 */
	public function create($model);

	/**
	 * @param Model $model
	 */
	public function update($model);

	/**
	 * @param int $id
	 */
	public function delete($id);
}