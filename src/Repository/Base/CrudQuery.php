<?php

namespace KiwiCore\Repository\Base;


trait CrudQuery
{
	public function create($model)
	{
		if ($model->save()) {
			return $model->id;
		}
		return -1;
	}

	public function update($model)
	{
		$model->save();
	}

	public function delete($id)
	{
		$this->query()->where("id", $id)->delete();
	}
}