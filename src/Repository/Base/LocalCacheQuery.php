<?php

namespace KiwiCore\Repository\Base;


use KiwiCore\Foundation\Environment;
use KiwiCore\Service\Web\LocalCache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;


trait LocalCacheQuery
{
	/**
	 * @return string
	 */
	abstract protected function cacheKey();

	/**
	 * @param Model $model
	 * @return \stdClass
	 */
	abstract protected function createItem(Model &$model);

	/**
	 * @param Collection $models
	 * @return Collection
	 */
	protected function sort(Collection $models)
	{
		return $models;
	}

	/**
	 * @var null|array
	 */
	private static $models = null;

	public function create($model)
	{
		$this->reset();
		if ($model->save()) {
			return $model->id;
		}
		return -1;
	}

	public function update($aliasLink)
	{
		$this->reset();
		$aliasLink->save();
	}

	public function delete($id)
	{
		$this->reset();
		$this->query()->where("id", $id)->delete();
	}

	private function reset()
	{
		self::$models = null;
		app(LocalCache::class)->remove($this->cacheKey());
	}

	/**
	 * @return array
	 */
	public function all()
	{
		if (self::$models !== null) {
            return self::$models;
		}
		$self = $this;
		self::$models = app(LocalCache::class)->get($this->cacheKey(), 30,
			function () use ($self) {
				$result = [];
				foreach ($this->query()->get() as $model) {
					$result[] = $self->createItem($model);
				}
				return $result;
			});
		return self::$models;
	}

	/**
	 * @param $id
	 * @return null|\stdClass|Model
	 */
	public function findById($id)
	{
		if (Environment::isAdmin()) {
			return $this->query()->find($id);
		}
		foreach ($this->all() as $link) {
			if ($id == $link->id) {
				return $link;
			}
		}
		return null;
	}

	/**
	 * @param int $start
	 * @param int $count
	 * @return array|Collection
	 */
	public function find($start = 0, $count = PHP_INT_MAX)
	{
		if (Environment::isAdmin()) {
			return $this->query()->skip($start)->take($count)->get();
		}
		return collect(array_slice($this->all(), $start, $count));
	}

	/**
	 * @return int
	 */
	public function count()
	{
		if (Environment::isAdmin()) {
			return $this->query()->count();
		}
		return count($this->all());
	}

}