<?php

namespace KiwiCore\Repository;

use KiwiCore\Condition\AntsCollectPublishedCondition;
use KiwiCore\Model\AntsCollectPublished;
use KiwiCore\Repository\Base\CrudQuery;
use KiwiCore\Repository\BaseRepository;

class AntsCollectPublishedRepository extends BaseRepository
{
    use CrudQuery;

    public function query()
    {
        return AntsCollectPublished::query();
    }

    public function all(){
        return $this->findByCondition(
            AntsCollectPublishedCondition::all()
        );
    }

    public function scheduleAutoGlobalPublished(){
        return $this->findByCondition(
            AntsCollectPublishedCondition::state(1)
        );
    }

}