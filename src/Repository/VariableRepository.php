<?php

namespace KiwiCore\Repository;


use KiwiCore\Model\Variable;
use KiwiCore\Repository\Base\CrudQuery;

class VariableRepository extends BaseRepository
{
	use CrudQuery;

	protected function query()
	{
		return Variable::query();
	}
}