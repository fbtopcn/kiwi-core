<?php

namespace KiwiCore\Model;


use DateTimeInterface;
use KiwiCore\Event\ModelCreated;
use KiwiCore\Event\ModelDeleted;
use KiwiCore\Event\ModelUpdated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use KiwiCore\Foundation\Environment;

class BaseModel extends Model
{
	protected $diffSite = false;

	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);

		switch (config('kiwi.site_type')){
            case 'independent':
                $this->independent();
                break;
            case 'complex':
                $this->complex();
                break;
            default:
                break;
        }

	}

    private function independent(){
	    //
    }

	private function complex(){
	    if (!$this->diffSite)
	        return ;

        if (Environment::isAdmin()){
            $site = request()->input('site');
            if (!empty($site))
                $this->table = $this->table.'_'.$site;
        } else {
            $site = config('kiwi.site');
            if (!empty($site))
                $this->table = $this->table.'_'.$site;
        }
    }
}