<?php

namespace KiwiCore\Model;

class AliasLink extends BaseModel
{
	const GLOBAL_NO_REPLACE = 0;
	const GLOBAL_REPLACE = 1;
    const GLOBAL_REDIRECT = 2;

	protected $table = "alias_link";
	protected $columns = [
		"id",
		"alias",
		"link",
		"global",
		"created_at",
		"updated_at",
	];
}