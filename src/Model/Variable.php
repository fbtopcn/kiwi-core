<?php

namespace KiwiCore\Model;

class Variable extends BaseModel
{
	protected $table = "variable";

	protected $columns = [
		"id",
		"name",
		"value",
		"description",
		"created_at",
		"updated_at",
	];

    protected $guarded = [];
}