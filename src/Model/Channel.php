<?php

namespace KiwiCore\Model;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
	protected $table = "channel";
    protected $guarded = [];
}