<?php

namespace KiwiCore\Model;
use Illuminate\Database\Eloquent\Model;

class Recommend extends Model
{
	const STATE_UN_PUBLISH = 0;
	const STATE_PUBLISHED = 1;
	const STATE_DELETED = 2;

	protected $table = "recommend";

	protected $columns = [
		"id",
		"grouping",
		"article_id",
		"cover",
		"created_at",
		"updated_at",
	];

}