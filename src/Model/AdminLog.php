<?php

namespace KiwiCore\Model;

class AdminLog extends BaseModel
{
	protected $table = "admin_log";
	protected $columns = [
		"id",
		"user",
		"uri",
		"action",
		"value",
		"created_at",
	];
}