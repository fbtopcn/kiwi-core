<?php

namespace KiwiCore\Model;

/**
 * Delete.
 * Trait RuntimeAttribute
 * @package KiwiCore\Model
 */
trait RuntimeAttribute
{
	// for pluck, use snake
//	protected $columns = null;
//
//	protected $camelColumnsInit = false;
//	protected $camelColumns = [];
//
//	/**
//	 * @var array
//	 */
//	protected $runtimeAttributes = [];
//
//	protected function isRuntimeAttribute($key)
//	{
//		$this->ensureCachedCamelColumn();
//		if ($this->columns === null || !is_array($this->columns)) {
//			return false;
//		}
//
//		return !in_array($key, $this->columns)
//			&& !in_array($key, $this->camelColumns);
//	}
//
//	protected function getRuntimeValue($key)
//	{
//		if ($this->hasGetRuntimeMutator($key)) {
//			return $this->getRuntimeMutator($key);
//		}
//		if (array_key_exists($key, $this->runtimeAttributes)) {
//			return $this->runtimeAttributes[$key];
//		} else {
//			return null;
//		}
//	}
//
//	protected function setRuntimeValue($key, $value)
//	{
//		if ($this->hasSetRuntimeMutator($key)) {
//			$this->setRuntimeMutator($key, $value);
//			return;
//		}
//		$this->runtimeAttributes[$key] = $value;
//	}
//
//	protected function putRuntimeValue($key, $value)
//	{
//		$this->runtimeAttributes[$key] = $value;
//	}
//
//	private function hasGetRuntimeMutator($key)
//	{
//		return method_exists($this, "get" . ucfirst($key) . "Attribute");
//	}
//
//	private function hasSetRuntimeMutator($key)
//	{
//		return method_exists($this, "set" . ucfirst($key) . "Attribute");
//	}
//
//	private function getRuntimeMutator($key)
//	{
//		return $this->{"get" . ucfirst($key) . "Attribute"}();
//	}
//
//	private function setRuntimeMutator($key, $value)
//	{
//		$this->{"set" . ucfirst($key) . "Attribute"}($value);
//	}
//
//	private function ensureCachedCamelColumn()
//	{
////		if (!$this->camelColumnsInit) {
////			$this->camelColumnsInit = true;
////
//////			if (!empty($this->columns)){
////                foreach ($this->columns as $column) {
////                    $camelColumn = camel_case($column);
////                    if ($camelColumn !== $column) {
////                        $this->camelColumns[] = $camelColumn;
////                    }
////                }
//////            }
////		}
//	}
}