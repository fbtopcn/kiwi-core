<?php

namespace KiwiCore\Model;

class Tdk extends BaseModel
{
    protected $diffSite = true;
    protected $table = 'tdk';
    protected $primaryKey = 'id';
    protected $guarded = [];
}