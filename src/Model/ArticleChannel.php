<?php

namespace KiwiCore\Model;

use Illuminate\Database\Eloquent\Model;

class ArticleChannel extends Model
{
	const STATE_UN_PUBLISH = 0;
	const STATE_PUBLISHED = 1;
	const STATE_DELETED = 2;

    public $timestamps = false;
	protected $table = "article_channel";
    protected $guarded = [];
}