<?php

namespace KiwiCore\Model;

use Illuminate\Database\Eloquent\Model;

class ArticleTag extends Model
{
    public $timestamps = false;
	protected $table = "article_tag";
    protected  $guarded = [];
}