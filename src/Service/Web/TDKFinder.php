<?php

namespace KiwiCore\Service\Web;


use KiwiCore\Repository\TdkRepository;
use KiwiCore\Foundation\Environment;
use Illuminate\Database\Eloquent\Model;

class TDKFinder
{
	/**
	 * @var TdkRepository
	 */
	private $tdkRepository;

	public function __construct()
	{
		$this->tdkRepository = app(TdkRepository::class);
	}

	/**
	 * @param string $urlPattern
	 * @param array $data
	 * @return \stdClass
	 */
	public function find($urlPattern, array $data = [])
	{
		// exists alias link
		$originalPath = Environment::$originalPath;
		if (!empty($originalPath)) {
			$originalPath = $this->withSlash($originalPath);
			$tdk = $this->tdkRepository->firstByPattern($originalPath, Environment::$domain);
			if (!empty($tdk)) {
				return $this->compile($tdk, $data);
			}

			if (Environment::$domain === Environment::DOMAIN_MOBILE) {
				$tdk = $this->tdkRepository->firstByPattern($originalPath, Environment::DOMAIN_PC);
				if (!empty($tdk)) {
					return $this->compile($tdk, $data);
				}
			}
		}

		$tdk = $this->tdkRepository->firstByPattern($urlPattern, Environment::$domain);
		if (!empty($tdk)) {
			return $this->compile($tdk, $data);
		}

		if (Environment::$domain === Environment::DOMAIN_MOBILE) {
			$tdk = $this->tdkRepository->firstByPattern($urlPattern, Environment::DOMAIN_PC);
			if (!empty($tdk)) {
				return $this->compile($tdk, $data);
			}
		}

		$tdk = new \stdClass();
		$tdk->title = "";
		$tdk->description = "";
		$tdk->keywords = "";
		return $tdk;
	}

	public function compile(\stdClass $tdk, array $data)
	{
		$values = [];
		foreach ($data as $key => $value) {
			$this->setValue($key, $value, $values);
		}
		foreach ($values as $key => $value) {
			$tdk->title = str_replace("{{{$key}}}", $value, $tdk->title);
			$tdk->description = str_replace("{{{$key}}}", $value, $tdk->description);
			$tdk->keywords = str_replace("{{{$key}}}", $value, $tdk->keywords);
		}

		$title = "";
		if (Environment::$domain === Environment::DOMAIN_PC) {
			$title = config("enle.title.pc");
		}
		if (Environment::$domain === Environment::DOMAIN_MOBILE) {
			$title = config("enle.title.mobile");
		}
		$tdk->title = str_replace("{{site}}", $title, $tdk->title);
		$tdk->description = str_replace("{{site}}", $title, $tdk->description);
		$tdk->keywords = str_replace("{{site}}", $title, $tdk->keywords);
		return $tdk;
	}

	private function setValue($name, $object, array &$values)
	{
		if ($object instanceof Model) {
			$object = $object->toArray();
		}

		if (is_object($object)) {
			foreach (get_object_vars($object) as $key => $value) {
				$this->setValue("$name.$key", $value, $values);
			}
		} else if (is_array($object)) {
			foreach ($object as $key => $value) {
				$this->setValue("$name.$key", $value, $values);
			}
		} else {
			$values[$name] = $object;
		}
	}

	private function withSlash($path)
	{
		if (!starts_with($path, "/")) {
			$path = "/$path";
		}
		if (!ends_with($path, "/") && !ends_with($path, ".html")) {
			$path = "$path/";
		}
		return $path;
	}
}