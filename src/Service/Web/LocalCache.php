<?php

namespace KiwiCore\Service\Web;


use Illuminate\Support\Facades\Cache;

class LocalCache
{
	public function put($key, $value, $expiredIn = 30)
	{
		if (app()->environment() === "production") {
			Cache::put($this->key($key), $value, $expiredIn);
		}
	}

	public function get($key, $expiredIn, \Closure $getter = null)
	{
		if (app()->environment() === "production") {
			return Cache::remember($this->key($key), $expiredIn, function () use ($getter) {
				return empty($getter) ? null : $getter();
			});
		}
		return empty($getter) ? null : $getter();
	}

	public function remove($key)
	{
		if (app()->environment() === "production") {
			Cache::forget($this->key($key));
		}
	}

	protected function key($key)
	{
		return $key;
	}
}