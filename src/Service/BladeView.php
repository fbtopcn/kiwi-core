<?php

namespace KiwiCore\Service;


use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Str;

class BladeView
{
	public static function boot()
	{
		Blade::directive("var", function ($expression) {
			if (!Str::contains($expression, ",")) {
				return "<?php  ?>";
			}
			$pos = mb_strpos($expression, ",");
			$key = mb_substr($expression, 0, $pos);
			$value = mb_substr($expression, $pos + 1);
			$key = trim($key, "\"");
			$value = trim($value);
			return "<?php \${$key} = $value ?>";
		});
	}
}