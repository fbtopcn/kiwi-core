<?php

namespace KiwiCore\Service;


use Exception;
use GuzzleHttp\Client;
use KiwiCore\Foundation\Logger;
use Upyun\Config;
use Upyun\Signature;
use Upyun\Upyun as UpYunService;
use Upyun\Util;

class UpYun
{
    public function token($path)
    {
        $config = $this->config();
        $data["save-key"] = $path;
        $data["expiration"] = time() + 30 * 60;
        $data["bucket"] = $config->bucketName;
        $policy = Util::base64Json($data);
        $signature = Signature::getBodySignature(
            $config,
            "POST",
            "/{$config->bucketName}",
            null,
            $policy);
        $signature = substr($signature, strlen("UpYun {$config->operatorName}:"));

        return (object)[
            "policy" => $policy,
            "authorization" => $signature,
        ];
    }

    public function info($url)
    {
        $upYun = new UpYunService($this->config());
        try {
            return $upYun->info($url);
        } catch (Exception $e) {
            Logger::error($e->getMessage());
            return false;
        }
    }

    public function purge(array $urls)
    {
        $upYun = new UpYunService($this->config());
        try {
            return $upYun->purge($urls);
        } catch (Exception $e) {
            Logger::error($e->getMessage());
            return false;
        }
    }

    public function imageInfo($url)
    {
        $client = new Client();
        try {
            $res = $client->request("GET", "$url!/info");
            $code = $res->getStatusCode();
            if ($code !== 200) {
                throw new Exception("get $url info, http status code $code");
            }
            $result = strval($res->getBody());
            if (empty($result)) {
                throw new Exception("get $url info, empty body");
            }
            $result = json_decode($result);
            return (object)[
                "width" => $result->width ?: 0,
                "height" => $result->height ?: 0,
            ];
        } catch (Exception $e) {
            Logger::error($e->getMessage());
            return (object)[
                "width" => 0,
                "height" => 0,
            ];
        }
    }

    /**
     * @param mixed $file stream or content
     * @param string $type
     * @param string $name
     * @return bool | string
     */
    public function upload($file, $type, $name = "")
    {
        if (empty($name)) {
            $name = $this->uuid();
        }

        $upYun = new UpYunService($this->config());
        try {
            $dir = str_replace(".", "/", $type);
            $targetPath = "/$dir/$name";
            $upYun->write($targetPath, $file);
            return image($targetPath);

        } catch (Exception $e) {
            Logger::error($e->getMessage());
            return false;
        }
    }

    private function config()
    {
        $bucketName = config("upyun.bucket");
        $operatorName = config("upyun.operator");
        $operatorPwd = config("upyun.password");
        return new Config($bucketName, $operatorName, $operatorPwd);
    }

    private function uuid()
    {
        return sprintf("%s,%s", date('m,d'), strtoupper(uniqid()));
    }
}