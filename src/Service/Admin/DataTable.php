<?php

namespace KiwiCore\Service\Admin;


use Closure;
use Illuminate\Support\Collection;

class DataTable
{
	/**
	 * @param array|Collection $list
	 * @param array $columns
	 * @param Closure|null $opFilter
	 * @return array|Collection
	 */
	public function build($list, array $columns, Closure $opFilter = null)
	{
		$defaultFilter = function () {
			return true;
		};
		$opFilter = $opFilter ?: $defaultFilter;

		foreach ($list as $row) {

			// 附着在row上，除了id的数据，如name
			$attachData = [];

			foreach ($columns as $column) {
				$name = $column["name"];
				switch ($column["type"]) {
					case "op":
						$row->$name = $this->setOperator($column, $row, $opFilter);
						break;
					case "child":
						$row->$name = $this->setChild($column, $row->$name, $opFilter);
						break;
					default:
						break;
				}
				if (isset($column["isAttach"])) {
					if (isset($column["field"])) {
						// TODO pointer value
					} else {
						$attachData[$name] = $row->$name;
					}
				}
			}
			if (isset($row->id)) {
				$row->DT_RowId = $row->id;
			}
			if (count($attachData) > 1) {
				$row->DT_RowAttach = json_encode($attachData);
			}
			if (count($attachData) == 1) {
				$row->DT_RowAttach = $attachData[array_keys($attachData)[0]];
			}
		}
		return $list;
	}

	private function setOperator($column, &$row, Closure $opFilter = null)
	{
		$name = $column["name"];
		$ops = [];
		foreach ($column["target"] as $op) {
			if ($opFilter($op, $row, $name) === true) {
				$ops[] = $op["name"];
			}
		}
		return $ops;
	}

	private function setChild($column, $list, Closure $opFilter = null)
	{
		foreach ($list as $row) {
			foreach ($column["target"] as $subColumn) {
				$name = $subColumn["name"];
				if ($subColumn["type"] === "op") {
					$row->$name = $this->setOperator($subColumn, $row, $opFilter);
					break;
				}
			}
			if (isset($row->id)) {
				$row->DT_RowId = $row->id;
			}
		}
		return $list;
	}

	public function getFilter($data, $columns, $filters)
	{
		$result = [];
		foreach ($data["columns"] as $column) {
			if ($column["searchable"] === "true") {
				$result[$column["name"]] = $column["search"]["value"];
			}
		}
		// TODO date & datetime
		// TODO column.field
		return $result;
	}
}