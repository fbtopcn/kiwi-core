<?php

namespace KiwiCore\Service\Admin;


use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

class Pager
{
	use ValidatesRequests;

	public function get(Request $request)
	{
		$this->validate($request, [
			"start" => "required|integer|min:0",
			"length" => "required|integer|max:100"
		]);
		return [intval($request["start"]), intval($request["length"])];
	}
}