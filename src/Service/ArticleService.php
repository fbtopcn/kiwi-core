<?php

namespace KiwiCore\Service;

use Illuminate\Support\Facades\Log;
use KiwiCore\Condition\AntsCollectDataCondition;
use KiwiCore\Condition\ChannelCondition;
use KiwiCore\Model\AntsCollectData;
use KiwiCore\Model\Article;
use KiwiCore\Repository\AntsCollectDataRepository;
use KiwiCore\Repository\ArticleChannelRepository;
use KiwiCore\Repository\ArticleRepository;
use KiwiCore\Repository\ChannelRepository;
use Illuminate\Support\Collection;
use KiwiCore\Repository\TagRelationRepository;
use KiwiCore\Repository\TagRepository;

class ArticleService
{
    public $repository;
    public $channelRepository;
    public $articleChannelRepository;

    public function __construct(
        ArticleRepository $repository,
        ChannelRepository $channelRepository,
        ArticleChannelRepository $articleChannelRepository
    )
    {
        $this->repository = $repository;
        $this->channelRepository = $channelRepository;
        $this->articleChannelRepository = $articleChannelRepository;
    }

    /**
     * @param Collection $articles
     * @return Collection
     */
    public function articleAddChannel(Collection &$articles){

        $channels = app(ChannelRepository::class)->findByCondition(
            ChannelCondition::channels($articles->pluck('channel')->toArray())
        )->keyBy('name');

        $articles->map(function (&$article) use ($channels) {
            $article->channel_title = isset($channels[$article->channel]) ? $channels[$article->channel]->title : '';
        });

        return $articles;
    }

    /**
     * 蚂蚁采集文章 单个发布/批量发布
     * @param $taskIds
     * @param $rule
     * @return bool|void
     */
    public function articlePublished(array $taskIds, $rule){

        $rule->published_type = is_string($rule->published_type)?$rule->published_type:'single';
        $rule->published_array = is_array($rule->published_array)?$rule->published_array:[];

        // 统计发布数量
        if ($rule->published_type == 'single'){
            $arrayChannels = $rule->published_array;
        } elseif($rule->published_type == 'all') {
            $channels = $this->channelRepository->all();
            $arrayChannels = [];
            foreach ($channels as $channel){
                $arrayChannels[$channel->name] = (object)['channel' => $channel->name, 'count' => 1];
            }
            foreach ($rule->published_array as $item){
                if (isset($arrayChannels[$item->channel])){
                    if ((int)$item->count == -1){
                        unset($arrayChannels[$item->channel]);
                    } else {
                        $arrayChannels[$item->channel]->count = (int)$item->count;
                    }
                }
            }
        } else {
            return ;
        }

        // 取发布的数据
        $number = array_sum(array_column($arrayChannels, 'count'));
        $articles = app(AntsCollectDataRepository::class)->findByCondition(
            AntsCollectDataCondition::stateComplete($taskIds),
            null,
            AntsCollectDataCondition::pagerBySequence(0, $number),
            AntsCollectDataCondition::sortByIdAsc()
        );
        if ($articles->isEmpty())
            return;

        // 发布
        foreach ($arrayChannels as $channel){
            $i = 0;
            while ($item = $articles->shift()){
                $article = new Article();
                $article->title = $item->title;
                $article->short_title = mb_substr($item->title, 0, 15);
                $article->content = $item->content;
                $article->image = $item->image;
                $article->state = 1;
                $article->channel = $channel->channel;
                $article->published_at = date('Y-m-d H:i:s');
                $article->created_at = date('Y-m-d H:i:s');
                $article->updated_at = date('Y-m-d H:i:s');
                $article->save();

                // 频道关系
                $this->articleChannelRepository->query()->insert([
                    'article_id' => $article->id,
                    'channel' => $channel->channel
                ]);

                // 标签关系
                $tags = [];
                $allTags = app(TagRepository::class)->all();
                $allTags->keyBy('title')->map(function ($val, $title) use ($article, &$tags){
                    strpos($article->content, $title) !== false && array_push($tags, $val->name);
                });
                app(TagRelationRepository::class)->create($article->id, array_unique($tags));

                // 状态
                app(AntsCollectDataRepository::class)->query()
                    ->where('id', $item->id)
                    ->update([
                        'state' => AntsCollectData::STATE_PUBLISHED,
                        'published_id' => $article->id
                    ]);
                $i++ ;

                if ($i >= $channel->count)
                    break;
            }
            Log::info(sprintf('Info: 发布频道 => %s, 发布数量 => %s篇, 实际发布 => %s篇', $channel->channel, $channel->count, $i));
        }

        return ;
    }
}