<?php

namespace KiwiCore\Service;


use KiwiCore\Exceptions\AppException;
use KiwiCore\Exceptions\ArgumentException;
use KiwiCore\Service\UpYun;
use Sunra\PhpSimple\HtmlDomParser;

class ArticleContent
{
	/**
	 * @var Link
	 */
	private $link;

	/**
	 * @var string
	 */
	private $firstImageUrl = "";

	public function __construct()
	{
		$this->link = app(Link::class);
	}

	public function getFirstImageUrl()
	{
		return $this->firstImageUrl;
	}

	public function pretreating($content)
	{
		return $this->traverse($content, function ($url) {
			if (!$this->link->isFull($url)) {
				throw new ArgumentException("image src cannot be relative link");
			}
			if (!$this->link->isFullImage($url)) {
				$upyun = app(UpYun::class);
				try {
					$imageContent = file_get_contents($url);
				} catch (\Exception $e) {
					throw new ArgumentException("image download fail $url");
				}
				if (empty($imageContent)) {
					throw new ArgumentException("image download fail $url");
				}
				$url = $upyun->upload($imageContent, "article");
				if (empty($url)) {
					throw new AppException("image upload fail $url");
				}
			}
			return $url;
		});
	}

	public function encode($content)
	{
		return $this->traverse($content, function ($url) {
			return $this->link->relativeImage($url);
		});
	}

	public function decode($content)
	{
		return $this->traverse($content, function ($url) {
			return $this->link->fullImage($url);
		});
	}

	private function traverse($content, \Closure $imageTreat = null)
	{
		if (empty($content)) {
			return "";
		}

		$dom = HtmlDomParser::str_get_html($content);
//		$dom = new Dom();
//		$dom->loadStr($content, []);
		$images = $dom->find("img");
		foreach ($images as &$image) {
			$url = $image->getAttribute("src");
			if (!empty($imageTreat)) {
				$url = $imageTreat($url);
			}
			$image->setAttribute('src', $url);
			if (empty($this->firstImageUrl)) {
				$this->firstImageUrl = $url;
			}
		}
		return (string)$dom;
	}
}