<?php

namespace KiwiCore\Service;

use KiwiCore\Exceptions\AppException;
use Upyun\Config;
use Upyun\Upyun as UpYunService;

class ImageUploadServer{

    private $config;
    private $driver;

    public function __construct()
    {
        $this->driver = config('image.driver');
        $this->config = config("image.store.$this->driver");
    }

    /**
     * @param $file
     * @param $path
     * @param string $name
     * @return string
     * @throws \Exception
     */
    public function upload($file, $path, $name = ''){
        try{
            $path = trim(str_replace(".", "/", $path), '/');
            if ($name == ''){
                $name = $this->uuid();
            }

            $function = "upload$this->driver";
            method_exists($this, $function) && ($src = $this->$function($file, $path, $name));
            if (!empty($src)){
                return $src;
            }

            return '';
        } catch (\Exception $e) {
            throw new \Exception("class::ImageUploadServer Image Update Fail File => $file Message => ".$e->getMessage(), 500);
        }
    }

    /**
     * @param $file
     * @param $path
     * @param $name
     * @return bool|string
     * @throws AppException
     */
    private function uploadFile($file, $path, $name){
        $fileContent = $this->getFileStream($file);

        $basePath = config("image.store.$this->driver.basePath");
        $filePath = sprintf('/uploads/%s/', $path);
        $fileName = $name.'.png';

        if (!is_dir($basePath.$filePath)){
            mkdir($basePath.$filePath,755, true);
        }

        if (file_put_contents($basePath.$filePath.$fileName, $fileContent)){
            return image($filePath.$fileName);
        }

        return '';
    }

    /**
     * @param $file
     * @param $path
     * @param $name
     * @return bool|mixed
     * @throws AppException
     */
    private function uploadUpYun($file, $path, $name){
        $fileContent = $this->getFileStream($file);

        $upYun = new UpYunService($this->upYunConfig());
        $targetPath = "/$path/$name";
        $upYun->write($targetPath, $fileContent);
        return image($targetPath);
    }

    private function getFileStream($file){
        if (isset(parse_url($file)['host'])) {
            $client = new \GuzzleHttp\Client();
            $fileContent = $client->request('GET', $file, ['verify' => false])->getBody()->getContents();
        } else {
            $fileContent = file_get_contents($file);
        }

        if (!$fileContent){
            throw new AppException("class::ImageUploadServer->getFileStream() GuzzleHttp Get or file_get_content Fail $file");
        }

        return $fileContent;
    }

    private function upYunConfig()
    {
        return new Config(
            $this->config['bucket'],
            $this->config['operator'],
            $this->config['password']
        );
    }

    private function uuid()
    {
        return sprintf("%s,%s", date('m,d'), strtoupper(uniqid()));
//        return sprintf("%04X%04X-%04X-%04X-%04X-%04X%04X%04X", mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }
}