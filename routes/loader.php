<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use KiwiCore\Foundation\Environment;

if (!function_exists("webRoute")) {
	function webRoute()
	{
        Route::middleware('cache.headers:public;max_age=1800;etag')->group(function () {
            // basisWebRoutes
            basisWebRoutes();

            // load domain route
            Environment::$domain == Environment::DOMAIN_PC && pcRoutes();
            Environment::$domain == Environment::DOMAIN_MOBILE && mobileRoutes();
            Environment::$domain == Environment::DOMAIN_CONSOLE && allWebRoutes();
        });
	}
}

if (!function_exists("adminRoute")) {
	function adminRoute()
	{
	    // 登录路由
		Route::middleware('admin')
            ->domain(config("kiwi.domain.admin"))
			->namespace("KiwiCore\\Http\\Controllers\\Admin")
			->group(function (){
                Route::group(["prefix" => "auth"], function () {
                    Auth::routes();
                });
            });

		// kiwi-core 路由
		Route::middleware('admin')
			->domain(config("kiwi.domain.admin"))
			->namespace("KiwiCore\\Http\\Controllers\\Admin")
			->group(__DIR__ . "/admin.php");

		// 业务后台路由
		$path = base_path('routes/admin.php');
		if (file_exists($path)) {
			Route::middleware('admin')
				->domain(config("kiwi.domain.admin"))
				->namespace("App\\Http\\Controllers\\Admin")
				->group($path);
		}
	}
}

if (!function_exists("basisWebRoutes")) {
    function basisWebRoutes()
    {
        // basisRoutes
        Route::middleware("web")
            ->domain(config('kiwi.domain.pc'))
            ->namespace("App\\Http\\Controllers\\Pc")
            ->group(routePath('web.php'));

        Route::middleware("web")
            ->domain(config('kiwi.domain.mobile'))
            ->namespace("App\\Http\\Controllers\\Mobile")
            ->group(routePath('web.php'));
    }
}
if (!function_exists("allWebRoutes")) {
    function allWebRoutes()
    {
        pcRoutes();
        mobileRoutes();
    }
}
if (!function_exists("pcRoutes")) {
    function pcRoutes()
    {
        Route::middleware("web")
            ->domain(config('kiwi.domain.pc'))
            ->namespace("App\\Http\\Controllers\\Pc")
            ->group(routePath('pc.php'));
    }
}
if (!function_exists("mobileRoutes")) {
    function mobileRoutes()
    {
        Route::middleware("web")
            ->domain(config('kiwi.domain.mobile'))
            ->namespace("App\\Http\\Controllers\\Mobile")
            ->group(routePath('mobile.php'));
    }
}
if (!function_exists("routePath")) {
    function routePath($file)
    {
        $path = rtrim("routes/".config('kiwi.site'), '/');
        return base_path($path.'/'.$file);
    }
}

// Run
switch (App::environment()) {
	case "local":
	case "test":
        webRoute();
		adminRoute();
		break;
	case "admin":
		adminRoute();
		break;
	default:
		webRoute();
		break;
}
